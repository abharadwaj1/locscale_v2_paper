import os 
import sys
import shutil
import subprocess

locscale_paper_main_path = "/tudelft/abharadwaj1/staff-umbrella/ajlab/AB/PhD_research/cryo_em_map_sharpening/papers_and_conference/elife_paper"
version_freeze_folder = "/tudelft/abharadwaj1/staff-umbrella/ajlab/AB/PhD_research/cryo_em_map_sharpening/papers_and_conference/elife_paper/Figure_versions/version_freeze"
png_input_folder = os.path.join(version_freeze_folder, 'png_format')
embedded_ai_input_folder = os.path.join(version_freeze_folder, 'embedded_ai_figures')
png_output_folder = os.path.join(locscale_paper_main_path, 'png_figures')
ai_output_folder = "/tudelft/abharadwaj1/staff-group/tnw/bn/AJ/Shared/manuscripts/2023_06_LocScale/Figures/versions_7_and_above/ai_files"
current_versions_list = [x for x in os.listdir(png_output_folder) if x.startswith('version_')]
current_version_numbers = [x.split('_')[-1] for x in current_versions_list]
current_version_numbers = [int(x) for x in current_version_numbers if x.isdigit()]
current_version_numbers.sort()
highest_version_number = current_version_numbers[-1]
print(f"Highest version number: {highest_version_number}")
print(f"New version number: {highest_version_number+1}")

proceed = input("Proceed? (y/n): ")
if proceed != 'y':
    new_version_folder = os.path.join(png_output_folder, f"version_{highest_version_number}")
    new_ai_version_folder = os.path.join(ai_output_folder, f"version_{highest_version_number}")
else:
    print("Proceeding.")
    new_version_folder = os.path.join(png_output_folder, f"version_{highest_version_number+1}")
    new_ai_version_folder = os.path.join(ai_output_folder, f"version_{highest_version_number+1}")
    if not os.path.exists(new_version_folder) and not os.path.exists(new_ai_version_folder):
        os.mkdir(new_version_folder)
        os.mkdir(new_ai_version_folder)
    else:
        print(f"Folder already exists. Exiting.")
        sys.exit()

    
# Copy png files
print("Copying png files.")
for file in os.listdir(png_input_folder):
    filepath = os.path.join(png_input_folder, file)
    if file.endswith('.png'):
        print(f"Copying {file} to {new_version_folder}")
        shutil.copy(filepath, new_version_folder)
    else:
        print(f"File {file} is not a png file. Skipping.")

# Copy ai files
print("Copying ai files.")
for file in os.listdir(embedded_ai_input_folder):
    filepath = os.path.join(embedded_ai_input_folder, file)
    if file.endswith('.ai'):
        print(f"Copying {file} to {new_ai_version_folder}")
        shutil.copy(filepath, new_ai_version_folder)
    else:
        print(f"File {file} is not an ai file. Skipping.")


# Rsync from project drive to local drive
sync_with_overleaf = input("Sync with overleaf? (y/n): ")
if sync_with_overleaf != 'y':
    print("Exiting.")
else:
    print("Proceeding.")
    print("Uploading to dropbox.")
    dropbox_folder = "/Apps/Overleaf/LocScale2_manuscript/Figures/"
    for file in os.listdir(new_version_folder):
        assert file.endswith('.png'), f"File {file} is not a png file."
        filepath = os.path.join(new_version_folder, file)
        cmd = ["bash","/home/abharadwaj1/bin/Dropbox-Uploader/dropbox_uploader.sh", "upload", filepath, dropbox_folder]
        print("Running command: ")
        print(' '.join(cmd))
        try: 
            subprocess.run(cmd, check=True)
        except subprocess.CalledProcessError as e:
            print("Error in uploading to dropbox. Exiting.")
            raise e
    print("Done.")
