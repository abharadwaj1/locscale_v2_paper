# Script to plot the wilson B-factor map for a given input map
# Take the path to a emmap from the user. Ask for the following arguments
# 1. Emmap path
# 2. Mask path (optional) (if not a spherical mask will be used around the molecule)
# 2. Output path for the wilson B-factor map (default: emmap_path + "_wilson_bfactor.mrc")
# 3. Resolution of the map (default: 3.5 A) or the local resolution map (atleast one of them should be provided)
# 4. Window size for the local resolution map, in angstorms (default: 25 A) 
# 5. Number of processors to use (default: 1)

import os 
import argparse
import numpy as np
from locscale.include.emmer.ndimage.profile_tools import compute_radial_profile, estimate_bfactor_standard, frequency_array, resample_1d
from locscale.include.emmer.ndimage.map_utils import load_map, save_as_mrc

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--emmap_path","-em", type=str, help="Path to the input map", required=True)
    parser.add_argument("--mask_path","-m", type=str, help="Path to the mask", required=False)
    parser.add_argument("--output_path","-o", type=str, help="Path to the output map", required=False)
    parser.add_argument("--resolution","-r", type=float, help="Resolution of the map", required=False)
    parser.add_argument("--local_resolution","-l", type=str, help="Path to the local resolution map", required=False)
    parser.add_argument("--window_size","-w", type=float, help="Window size (in angstorms)", required=True)
    parser.add_argument("--nproc","-n", type=int, help="Number of processors to use", default=1)
    parser.add_argument("--wilson_cutoff","-wc", type=float, help="Wilson cutoff", default=10)
    args = parser.parse_args()
    return args

def check_args(args):
    # Check if the input map exists
    if not os.path.exists(args.emmap_path):
        raise Exception("Input map does not exist")
    # Check if the mask exists
    if args.mask_path is not None:
        if not os.path.exists(args.mask_path):
            raise Exception("Mask does not exist")
    # Check if local resolution map exists
    if args.local_resolution is not None:
        if not os.path.exists(args.local_resolution):
            raise Exception("Local resolution map does not exist")
    # Check whether resolution or local resolution map is provided
    if args.resolution is None and args.local_resolution is None:
        raise Exception("Provide either the resolution or the local resolution map. Use --resolution or --local_resolution flag")
    

def get_wilson_bfactor_map(args):
    # Load the map
    emmap_path = args.emmap_path
    emmap, apix = load_map(emmap_path)

    mask = get_mask_from_args(args, emmap.shape, apix)

    # get the resolution
    resolution = get_resolution_from_args(args, emmap.shape, apix)

    # get the window size
    wn = float(args.window_size)

    # get the number of processors
    nproc = int(args.nproc)
    wilson_cutoff = float(args.wilson_cutoff)
    print("Window size: ", wn)
    print("Number of processors: ", nproc)
    print("apix: ", apix)
    print("Wilson cutoff: ", wilson_cutoff)

    # get the wilson bfactor map
    wilson_bfactor_map = get_wilson_bfactor_map_from_emmap(emmap, mask, apix, resolution, wn, wilson_cutoff, nproc)

    # save the map
    output_path = get_output_path_from_args(args, emmap_path)

    save_as_mrc(wilson_bfactor_map, output_path, apix)

    print("Wilson B-factor map saved at: ", output_path)

def get_mask_from_args(args, shape, apix):
    if args.mask_path is None:
        mask_radius = int(shape[0]//2 * 0.9)
        mask = get_spherical_mask(shape, mask_radius)
    else:
        mask, _ = load_map(args.mask_path)
        mask = (mask > 0.99).astype(np.int_)
    return mask

def get_resolution_from_args(args, shape, apix):
    if args.resolution is not None:
        resolution = float(args.resolution)
    else:
        local_resolution, _ = load_map(args.local_resolution)
        resolution = local_resolution
    return resolution

def get_output_path_from_args(args, emmap_path):
    if args.output_path is None:
        output_path = emmap_path[:-4] + "_wilson_bfactor.mrc"
    else:
        output_path = args.output_path
    return output_path

def get_spherical_mask(shape, radius):
    mask = np.zeros(shape)
    x, y, z = np.indices(shape)
    mask[(x-shape[0]//2)**2 + (y-shape[1]//2)**2 + (z-shape[2]//2)**2 <= radius**2] = 1
    return mask

def compute_wilson_bfactor_window(vol, apix, wilson_cutoff, fsc_cutoff):
    rp_in = compute_radial_profile(vol)
    freq_in = frequency_array(rp_in, apix)
    xnew, ynew = resample_1d(freq_in**2, np.log(rp_in), num=100)
    rp = np.exp(ynew)
    freq = np.sqrt(xnew)
    bfactor = estimate_bfactor_standard(freq, rp, wilson_cutoff=wilson_cutoff, fsc_cutoff=fsc_cutoff, standard_notation=True)
    return bfactor

def get_local_wilson_bfactor_emmap(emmap, voxel, apix, wn_voxels, wilson_cutoff, fsc_cutoff):
    from locscale.include.emmer.ndimage.map_utils import extract_window, resample_map
    # silence the warnings
    import warnings
    # get the window
    window = extract_window(emmap, voxel, wn_voxels)

    if fsc_cutoff > 7:
        fsc_cutoff = 7
    
    # catch OptimizeWarning
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        bfactor = compute_wilson_bfactor_window(window, apix, wilson_cutoff, fsc_cutoff)

        return (voxel, bfactor)


def get_wilson_bfactor_map_from_emmap(emmap, mask, apix, resolution, wn, wilson_cutoff, nproc):
    from locscale.include.emmer.ndimage.map_utils import get_all_voxels_inside_mask
    import joblib
    from scipy.ndimage import uniform_filter
    from tqdm import tqdm

    # get the voxels inside the mask
    voxels_inside_mask = get_all_voxels_inside_mask(mask)

    # get the window size in voxels
    wn_voxels = int(round((wn / apix)))

    if isinstance(resolution, float):
        fsc_cutoff = resolution
        
        use_local_resolution = False
    elif isinstance(resolution, np.ndarray):
        use_local_resolution = True
        
        local_resolution_array = {tuple(k): resolution[k[0],k[1],k[2]] for k in voxels_inside_mask}
    
    # get the wilson bfactor map

    wilson_bfactor_map = np.zeros(emmap.shape)

    if use_local_resolution:

        results = joblib.Parallel(n_jobs=nproc)(joblib.delayed(get_local_wilson_bfactor_emmap)(\
            emmap, voxel, apix, wn_voxels, wilson_cutoff, local_resolution_array[tuple(voxel)]) \
            for voxel in tqdm(voxels_inside_mask))

        for voxel, bfactor in results:
            wilson_bfactor_map[voxel[0],voxel[1],voxel[2]] = bfactor
        
    else:
        results = joblib.Parallel(n_jobs=nproc)(joblib.delayed(get_local_wilson_bfactor_emmap)(\
            emmap, voxel, apix, wn_voxels, wilson_cutoff, fsc_cutoff) \
            for voxel in tqdm(voxels_inside_mask))
        
        for voxel, bfactor in results:
            wilson_bfactor_map[voxel[0],voxel[1],voxel[2]] = bfactor
    
    return wilson_bfactor_map

def print_args(args):
    print("Arguments:")
    for arg in vars(args):
        print(arg, getattr(args, arg))

def main():
    print("Running Wilson B-factor map")
    print("Arguments:")
    args = get_args()
    print("====================================")
    print_args(args)
    check_args(args)
    get_wilson_bfactor_map(args)
    print("Done!")
    print("====================================")

if __name__ == "__main__":
    main()








