import os 
import subprocess
import numpy as np
from locscale.include.emmer.ndimage.map_utils import load_map, save_as_mrc 

unsharpened_map_path = "/home/abharadwaj1/papers/elife_paper/figure_information/data/maps/emd_3061/model_free/EMD_3061_unfiltered.mrc"

output_folder = "/home/abharadwaj1/papers/elife_paper/figure_information/outputs/running_model_free_multiple"

number_of_iterations = 25 

def copy_files_to_folder(file, folder):
    import shutil
    if not os.path.exists(folder):
        os.makedirs(folder)
    newfilepath = os.path.join(folder, os.path.basename(file))
    newpath = shutil.copyfile(file, newfilepath)
    
    return newpath

def get_model_free_command(map_file, output_name):
    cmd = []
    cmd.append("python")
    cmd.append("/home/abharadwaj1/dev/locscale/locscale/main.py")
    cmd.append("-em")
    cmd.append(map_file)
    cmd.append("-v")
    cmd.append("-o")
    cmd.append(output_name)
    cmd.append("-np")
    cmd.append("4")
    cmd.append("--build_using_pseudomodel")
    cmd.append("-res")
    cmd.append("3.4")
    return cmd

def run_model_free_command(cmd):
    import subprocess
    try:
        p = subprocess.run(cmd, check=True)
    except Exception as e:
        raise e    

def find_model_map_in_folder(folder):
    all_files = os.listdir(folder)
    format: "_4locscale.mrc"
    modelmap_path = [x for x in all_files if x.endswith("_4locscale.mrc")][0]
    return os.path.join(folder, modelmap_path)

def find_confidence_map_in_folder(folder):
    all_files = os.listdir(folder)
    format: "_confidenceMap.mrc"
    confidence_path = [x for x in all_files if x.endswith("_confidenceMap.mrc")][0]
    return os.path.join(folder, confidence_path)

model_maps_iteration = {}
            
for i in range(number_of_iterations):
    iteration_folder = os.path.join(output_folder, "iteration_{}".format(i))
    copied_path = copy_files_to_folder(unsharpened_map_path, iteration_folder)
    output_file = os.path.join(iteration_folder, "model_free_iteration_{}.mrc".format(i))
    
    command_iteration = get_model_free_command(copied_path, output_file)
    run_model_free_command(command_iteration)
    
    print("Finished iteration {}".format(i))
    
    processing_files_folder = os.path.join(iteration_folder, "processing_files")
    modelmap_path = find_model_map_in_folder(processing_files_folder)
    
    model_maps_iteration[i] = modelmap_path

model_maps = []
for i in range(number_of_iterations):
    modelmap, apix = load_map(model_maps_iteration[i])
    model_maps.append(modelmap)

mean_model_map = np.mean(model_maps, axis=0)
var_model_map = np.var(model_maps, axis=0)

save_as_mrc(mean_model_map, os.path.join(output_folder, "mean_model_map.mrc"), apix=apix)
save_as_mrc(var_model_map, os.path.join(output_folder, "var_model_map.mrc"), apix=apix)

confidence_mask_path = find_confidence_map_in_folder(processing_files_folder)

final_locscale_command = []
final_locscale_command.append("python")
final_locscale_command.append("/home/abharadwaj1/dev/locscale/locscale/main.py")
final_locscale_command.append("-em")
final_locscale_command.append(unsharpened_map_path)
final_locscale_command.append("-ma")
final_locscale_command.append(confidence_mask_path)
final_locscale_command.append("-mm")
final_locscale_command.append(os.path.join(output_folder, "mean_model_map.mrc"))
final_locscale_command.append("-v")
final_locscale_command.append("-o")
final_locscale_command.append(os.path.join(output_folder, "final_locscale.mrc"))
final_locscale_command.append("-np")
final_locscale_command.append("4")
final_locscale_command.append("-res")
final_locscale_command.append("3.4")

run_model_free_command(final_locscale_command)
    
    
    
    
    
    
