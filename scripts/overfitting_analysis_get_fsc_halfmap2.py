# %%
from locscale.include.emmer.ndimage.fsc_util import calculate_fsc_maps
from locscale.preprocessing.headers import run_servalcat_iterative
from locscale.include.emmer.pdb.pdb_to_map import pdb2map
from locscale.include.emmer.ndimage.map_utils import save_as_mrc, load_map
from locscale.include.emmer.ndimage.filter import get_cosine_mask
import os
import numpy as np


# %%
refmac_iterations = np.arange(1, 50, dtype=int)
def copy_files_to_folder(file, folder):
    import shutil
    copied_file_path = shutil.copy(file, folder)
    return copied_file_path
    
parent_folder = "/home/abharadwaj1/papers/elife_paper/figure_information/data/pseudomodel_during_iterations/hybrid_pseudomodel_iterations/overfitting_analysis"
model_path = [os.path.join(parent_folder, x) for x in os.listdir(parent_folder) if x.endswith('.cif')][0]
halfmap1_path = [os.path.join(parent_folder, x) for x in os.listdir(parent_folder) if "half_map_1" in x][0]
halfmap2_path = [os.path.join(parent_folder, x) for x in os.listdir(parent_folder) if "half_map_2" in x][0]
confidence_mask_path = [os.path.join(parent_folder, x) for x in os.listdir(parent_folder) if "confidence" in x][0]

num_iter = 50
resolution = 2.5

halfmap2,apix = load_map(halfmap2_path)
halfmap1,apix = load_map(halfmap1_path)
mask = load_map(confidence_mask_path)[0]
mask_binarized = (mask >= 0.99).astype(np.int_)
softmask = get_cosine_mask(mask_binarized,3)

input_folder = os.path.dirname(model_path)
output_folder = os.path.join(input_folder, 'refmac_iteration_{}'.format(num_iter))

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

copied_map_path = copy_files_to_folder(halfmap1_path, output_folder)
copied_map2_path = copy_files_to_folder(halfmap2_path, output_folder)
copied_model_path = copy_files_to_folder(model_path, output_folder)

# refined_model_path = run_servalcat_iterative(
#     model_path=copied_model_path, map_path=copied_map_path, pseudomodel_refinement=True,
#     resolution=resolution, num_iter=num_iter)

refined_model_per_iteration = {k : os.path.join(output_folder,f"servalcat_refinement_cycle_{k}.pdb") for k in refmac_iterations}
print("Calculating FSC values")
fsc_average_halfmap2 = {}
fsc_average_halfmap1 = {}
for i, refined in refined_model_per_iteration.items():
    refined_model_map = pdb2map(refined, apix=apix, size=mask.shape)
    save_as_mrc(refined_model_map, os.path.join(output_folder, f"refined_model_map_{i}.mrc"), apix=apix)

    fsc_vals_masked = calculate_fsc_maps(refined_model_map*softmask, halfmap2*softmask)
    fsc_vals_unmasked = calculate_fsc_maps(refined_model_map, halfmap2)

    fsc_vals_masked_halfmap1 = calculate_fsc_maps(refined_model_map*softmask, halfmap1*softmask)
    fsc_vals_unmasked_halfmap1 = calculate_fsc_maps(refined_model_map, halfmap1)
    fsc_average_halfmap2[i] = {
        'masked' : (fsc_vals_masked, np.mean(fsc_vals_masked)),
        'unmasked' : (fsc_vals_unmasked, np.mean(fsc_vals_unmasked))
    }
    fsc_average_halfmap1[i] = {
        'masked' : (fsc_vals_masked_halfmap1, np.mean(fsc_vals_masked_halfmap1)),
        'unmasked' : (fsc_vals_unmasked_halfmap1, np.mean(fsc_vals_unmasked_halfmap1))
    }
    print(f"iteration{i} : masked FSC {fsc_average_halfmap2[i]['masked'][1]:.3f}  and unmasked FSC {fsc_average_halfmap2[i]['unmasked'][1]:.3f}")

# Dump the fsc values to a file in the input folder
import pickle
with open(os.path.join(input_folder, 'fsc_average_halfmap_2_cycle50.pickle'), 'wb') as f:
    pickle.dump(fsc_average_halfmap2, f)

with open(os.path.join(input_folder, 'fsc_average_halfmap_1_cycle50.pickle'), 'wb') as f:
    pickle.dump(fsc_average_halfmap1, f)



    


# %%
# 


