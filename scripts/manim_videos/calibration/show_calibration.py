from manim import * 
import os 
import numpy as np
from emmer.ndimage.map_utils import load_map 
import pickle 
np.random.seed(0)
class CalibrationVariance(Scene):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
        # define some variables
        
        # input paths 
        self.monte_carlo_cubes_dir = "/home/abharadwaj1/papers/elife_paper/figure_information/data/manim_videos/monte_carlo_samples/9112_cube_15"
        self.input_window_path = os.path.join(self.monte_carlo_cubes_dir, "input_map.mrc")
        self.locscale_window_path = os.path.join(self.monte_carlo_cubes_dir, "locscale_window.mrc")
        self.locscale_window = load_map(self.locscale_window_path)[0]
        self.input_window = load_map(self.input_window_path)[0]
        self.mean_path = os.path.join(self.monte_carlo_cubes_dir, "mean_map.mrc")
        self.var_path = os.path.join(self.monte_carlo_cubes_dir, "var_map.mrc")
        self.monte_carlo_samples_path = [os.path.join(self.monte_carlo_cubes_dir, "monte_carlo_sample_{}.mrc".format(i)) for i in range(15)]
        self.monte_carlo_samples = [load_map(path)[0] for path in self.monte_carlo_samples_path]
        self.slice_index = 16 
        self.central_slices = [sample[:,:,self.slice_index] for sample in self.monte_carlo_samples]
        self.voxel_pos = np.array([12,22,self.slice_index])
        self.voxel_intensities = [sample[self.voxel_pos[0], self.voxel_pos[1], self.voxel_pos[2]] for sample in self.monte_carlo_samples]
        
        self.slice_indices_to_show = [2, 4, 8, 12, 14]
        self.corner_indices = []
        self.num_bins = 10
        self.locscale_slice = self.locscale_window[:,:,self.slice_index]
        self.input_window_slice = self.input_window[:,:,self.slice_index]
        self.isotonic_regressor_path = "/home/abharadwaj1/dev/locscale/locscale/utils/calibrator_locscale_target.pkl"
        self.isotonic_regressor = pickle.load(open(self.isotonic_regressor_path, "rb"))
        
    def hide_all_mobjects(self):
        for mob in self.mobjects:
            mob.set_opacity(0)
    
    def show_all_mobjects(self):
        for mob in self.mobjects:
            mob.set_opacity(1)
            
    def normalize(self, x):
        return (x - np.min(x)) / (np.max(x) - np.min(x)) * 255
            
    def construct(self):
        # Display the slices 
        # Title
        title_1 = Text("Model Calibration", font_size=28).move_to(ORIGIN).shift(UP * 0.5)
        title_2 = Text("for Monte Carlo Dropout", font_size=28).next_to(title_1, DOWN, buff=0.1)       
        
        title = VGroup(title_1, title_2)
        self.play(Write(title))
        self.wait(1)
        self.hide_all_mobjects()
        self.play(Text("Hidden").move_to(ORIGIN))
        
        self.play(FadeOut(title))
        
        slice_object = self.display_slices(self.central_slices[1], label="Mean prediction")
        self.play(FadeIn(slice_object))
        self.wait(1)
        
        # # Display the voxel for the slices
        image_slice = self.central_slices[1]
        # select 25 voxels from the image slice where the image intensity is greater than 0.1
        image_slice_mask = image_slice > 0.15
        voxels_in_slice = np.asarray(np.where(image_slice_mask)).T.tolist()
        buffer_size = 4
        point_within_buffer = lambda x: x[0] > buffer_size and x[0] < 32 - buffer_size and x[1] > buffer_size and x[1] < 32 - buffer_size
        voxels_in_slice = [voxel for voxel in voxels_in_slice if point_within_buffer(voxel)]
        # choose 25 random voxels from the list of voxels
        np.random.seed(0)
        random_voxel_indices = np.random.choice(len(voxels_in_slice), 25, replace=False)
        random_voxels = [voxels_in_slice[i] for i in random_voxel_indices] 
        
        # For each voxel, display the voxel position and the voxel intensity distribution
        # the distribution should be shown as 25 histograms placed on a 5 x 5 grid
        
        grid_positions, grid_group, grids = self.get_grid_positions(5, 5, size_grid=(1.2,1.2))
        # display grids 
        self.play(FadeIn(grid_group))
        self.wait(1)
        
        all_curves = []
        all_mean_values = []
        all_dot_objects = []
        for i, voxel_position in enumerate(random_voxels):    
            dot_object = self.display_voxel_dots(slice_object, voxel_position[::-1])
            voxel_intensities = [sample[voxel_position[0], voxel_position[1], self.slice_index] for sample in self.monte_carlo_samples]
            curve, x_line, mean, std = self.display_normal_fit(voxel_intensities, bins=self.num_bins, max_width=1.2, max_height=0.4, calibrate=False)
            curve.move_to(grids[i].get_bottom(), aligned_edge=DOWN)
            self.play(FadeIn(curve), FadeIn(dot_object))
            all_mean_values.append((mean, std))
            self.wait(0.05)
            all_curves.append(curve)
            all_dot_objects.append(dot_object)
                    
        self.wait(2)
        
        all_dot_objects_group = VGroup(*all_dot_objects)
        
        # Display the input window slice 
        input_slice_object = self.display_slices(self.input_window_slice, label="input")
        input_slice_object.next_to(slice_object, RIGHT, buff=0.5)
        self.play(FadeIn(input_slice_object))
        self.wait(2)
        
        
        # Create arrows from the input slice to the locscale slice and slice_object to locscale_slice_object
        line_mean_to_input = Line(slice_object.get_bottom(), input_slice_object.get_bottom(), buff=0.1).shift(DOWN * 0.1)
        # Display the locscale window slice
        locscale_slice_object = self.display_slices(self.locscale_slice, label="locscale")
        locscale_slice_object.shift(DOWN * 2.5 + RIGHT * 1.2)
        arrow_to_locscale = Arrow(line_mean_to_input.get_center(), locscale_slice_object.get_top(), buff=0)
        
        self.play(
            FadeIn(line_mean_to_input),
            GrowArrow(arrow_to_locscale),
            FadeIn(locscale_slice_object)
        )
        self.wait(2)
        
        locscale_dot_objects_group_copy = all_dot_objects_group.copy()
        locscale_dot_objects_group_copy.shift(DOWN * 2.5 + RIGHT * 1.2)
        # change the color of the dots to YELLOW 
        locscale_dot_objects_group_copy.set_color(YELLOW)
        self.play(ReplacementTransform(all_dot_objects_group.copy(), locscale_dot_objects_group_copy))
        
        # for i, voxel_position in enumerate(random_voxels):
        #     voxel_intensities = [sample[voxel_position[0], voxel_position[1], self.slice_index] for sample in self.monte_carlo_samples]
        #     shaded_curve, shaded_x_line, mean, std = self.display_normal_fit(voxel_intensities, bins=self.num_bins, max_width=1.2, max_height=0.4, ci=True)
        #     shaded_curve.move_to(grids[i].get_bottom(), aligned_edge=DOWN)
        #     all_curves_shaded_ci.append(shaded_curve)
        # self.play(*[FadeIn(curve) for curve in all_curves_shaded_ci])
        # self.wait(2)
        
        locscale_value_list = []
        all_colors = []
        z_scores = []
        for i, voxel_position in enumerate(random_voxels):
            shaded_curve_voxel = all_curves[i]
            locscale_value_at_this_voxel = self.locscale_window[voxel_position[0], voxel_position[1], self.slice_index]
            
            locscale_value_line = self.display_target_value_in_curve(shaded_curve_voxel, locscale_value_at_this_voxel, max_width=1.2, max_height=0.4)
            locscale_value_list.append(locscale_value_line)
            mean_value_this_voxel = all_mean_values[i][0]
            std_value_this_voxel = all_mean_values[i][1]
            z_score_this_voxel = (locscale_value_at_this_voxel - mean_value_this_voxel) / (std_value_this_voxel)
            z_score_this_voxel = np.clip(z_score_this_voxel, -3, 3)
            z_scores.append(z_score_this_voxel)
            if abs(z_score_this_voxel) < 1.96:
                all_colors.append(GREEN)
            else:
                all_colors.append(RED)

        self.play(*[FadeIn(line) for line in locscale_value_list])        
        self.wait(2)
        
        self.play(*[FadeOut(line) for line in locscale_value_list])
        self.play(*[FadeOut(curve) for curve in all_curves])
        
        # convert everything to standard normal and then plot the z scores 
        all_curves_shaded_ci = []
        for i, voxel_position in enumerate(random_voxels):
            z_score_this_voxel = z_scores[i]
            shaded_curve, shaded_x_line = self.display_standard_normal(max_width=1.2, max_height=3.6, ci=True)
            shaded_curve.move_to(grids[i].get_bottom(), aligned_edge=DOWN)
            all_curves_shaded_ci.append(shaded_curve)
        self.play(*[FadeIn(curve) for curve in all_curves_shaded_ci])
        self.wait(2)
        
        # now let us plot the z scores line on the standard normal curve
        z_score_lines = []
        for i, voxel_position in enumerate(random_voxels):
            z_score_this_voxel = z_scores[i]
            
            shaded_curve = all_curves_shaded_ci[i]
            z_score_line = self.display_target_value_in_curve(shaded_curve, z_score_this_voxel, max_width=1.2, max_height=3.6,\
                                                                minval=-3, maxval=3)
            z_score_lines.append(z_score_line)
        
        self.play(*[FadeIn(line) for line in z_score_lines])
        self.wait(1)
        # # set the bounding box color for each grid based on the color of the curve
        for i, grid in enumerate(grids):
            grid[0].set_color(all_colors[i])
            # print the name of the color for this grid
            color_name = "Green" if all_colors[i] == GREEN else "Red"
            print("Grid {} is {}".format(i, color_name))
        self.wait(2)
        
        # Display the percentage of grids that are green and red
        num_green = len([color for color in all_colors if color == GREEN])
        percent_green = num_green / len(all_colors) * 100
        expected_probability_text = Text("Expected \nprobability: {:.2f}%".format(95), font_size=22)
        observed_probability_text = Text("Observed \nprobability: {:.2f}%".format(percent_green), font_size=22)
        expected_probability_text.next_to(slice_object, UP, buff=2).shift(RIGHT * 1)
        observed_probability_text.next_to(expected_probability_text, DOWN, buff=0.5)
        
        self.play(Write(expected_probability_text), Write(observed_probability_text))
        self.wait(2)
        
        # Calibration text 
        
        
    def display_text(self, text, position=ORIGIN, font_size=22):
        text_object = Text(text, font_size=font_size)
        text_object.move_to(position)
        self.add(text_object)
        return text_object
    
    def display_target_value_in_curve(self, curve, target_value, max_width=1.2, max_height=0.8, minval=0, maxval=1):
        x_position_left_end = curve.get_left()[0]
        x_position_right_end = curve.get_right()[0]
        y_position_bottom = curve.get_bottom()[1]
        y_position_top = curve.get_top()[1]
        
        # rescale the target value to the range of the curve
        target_value_rescaled = (target_value - minval) / (maxval - minval) * (x_position_right_end - x_position_left_end) + x_position_left_end
        line = DashedLine(np.array([target_value_rescaled, y_position_bottom, 0]), np.array([target_value_rescaled, y_position_top, 0]), color=YELLOW)
        return line
        
        
        
    def get_grid_positions(self, num_rows, num_cols, size_grid):
        # get the grid positions for the given number of rows and columns
        # give the center position of each grid
        frame = Rectangle(height=num_rows * size_grid[0] * 1.2, width=num_cols * size_grid[1] * 1.2)
        frame.move_to(ORIGIN).shift(LEFT * 2 + DOWN * 0.75)
        grids = []
        positions = []
        grid_row = []
        for i in range(num_rows):
            grid_col = []
            for j in range(num_cols):
                grid_square = Rectangle(height=size_grid[0], width=size_grid[1])
                grid_square.add(SurroundingRectangle(grid_square, buff=0))
                
                if len(grid_col) > 0:
                    grid_square.next_to(grid_col[-1], RIGHT, buff=0.2)
                else:
                    grid_square.move_to(frame.get_corner(UL), aligned_edge=LEFT)
                    grid_square.shift(DOWN * i * size_grid[0] * 1.2)
                grid_col.append(grid_square)
                
                positions.append(grid_square.get_center())
                grids.append(grid_square)                        

        grids_group = VGroup(*grids)
        return positions, grids_group, grids
        
    def display_slices(self, im_slice, label):
        # Display the slices 

        slice_object = ImageMobject(self.normalize(im_slice))
        slice_object.scale(5)
        
        # add a bounding box to the image
        slice_object.add(SurroundingRectangle(slice_object, buff=0))
        # add a text showing the slice index
        sample_index_text = Text("{}".format(label), font_size=22)
        slice_object.add(sample_index_text.next_to(slice_object, UP, buff=0.1))
        slice_object.move_to(ORIGIN)
        slice_object.shift(RIGHT * 3.5)
        self.corner_indices.append(slice_object.get_corner(UL))
        
        self.add(slice_object)
        return slice_object
        
    def display_voxel_dots(self, slice_object, voxel_pos):
        # Display the voxel for the slices
        dot_object = Dot(color=RED).scale(0.5)
        dot_object.move_to(self.get_voxel_position_from_object(slice_object, index_slice=1, voxel_pos=voxel_pos))
        self.add(dot_object)
        return dot_object    
    
    def display_normal_fit(self, data, bins=10, max_height=3, max_width=0.5, gap=0.2, ci=False, calibrate=True):
        """
        Display a histogram for the given data.

        Parameters:
        - data: Dataset for which the histogram is to be created.
        - bins: Number of bins for the histogram.
        - max_height: Maximum height for a bar in the scene.
        - bar_width: Width of each bar in the scene.
        - gap: Gap between consecutive bars.
        """
        from scipy.stats import norm
        import warnings
        warnings.filterwarnings("ignore")
        # Compute histogram
        hist, bin_edges = np.histogram(data, bins=bins)

        # Scale the heights to fit within our max_height
        scaled_hist = max_height * hist / np.max(hist)

        
        mean = np.mean(data)
        variance = np.var(data)
        std = np.sqrt(variance)
        
        se_uncalibrated = std / np.sqrt(15) 
        if calibrate:
            se = self.isotonic_regressor.predict([se_uncalibrated])[0]
        else:
            se = se_uncalibrated 
            
        CI_limits = [mean - 1.96 * se, mean + 1.96 * se]
        
        x = np.linspace(0.05, 0.5, 100) 
        y = norm.pdf(x, mean, se)
        y = max_height * y / np.max(y)
        
        curve = VGroup() # set color to RED
        curve.set_color(RED)
        
        for i in range(len(x) - 1):
            if x[i] >= CI_limits[0] and x[i+1] <= CI_limits[1]:
                color = GREEN
            else:
                color = RED
                
            if ci==True:
                line = Line(np.array([x[i], y[i], 0]), np.array([x[i+1], y[i+1], 0]), color=color)
            else:
                line = Line(np.array([x[i], y[i], 0]), np.array([x[i+1], y[i+1], 0]), color=BLUE)
            curve.add(line)
            
        # scale the curve to fit within the max_width
        curve.scale(max_width / curve.get_width())
        
        # add a line below the curve 
        x_line = Line(np.array([x[0], 0, 0]), np.array([x[-1], 0, 0]), color=WHITE)
        
        x_line.scale(max_width / x_line.get_width())
        
        x_line.next_to(curve, DOWN, buff=0.1)
        
        curve.add(x_line)
        
    
        return curve, x_line, mean, se
    
    def display_standard_normal(self, max_height=3, max_width=0.5, ci=True):
        """
        Display a histogram for the given data.

        Parameters:
        - data: Dataset for which the histogram is to be created.
        - bins: Number of bins for the histogram.
        - max_height: Maximum height for a bar in the scene.
        - bar_width: Width of each bar in the scene.
        - gap: Gap between consecutive bars.
        """
        from scipy.stats import norm
        import warnings
        warnings.filterwarnings("ignore")
        
                
        x = np.linspace(-3, 3, 100) 
        y = norm.pdf(x, 0, 1)
        y = max_height * y / np.max(y)
        
        curve = VGroup() # set color to RED
        curve.set_color(RED)
        CI_limits = [-1.96, 1.96]
        
        for i in range(len(x) - 1):
            if x[i] >= CI_limits[0] and x[i+1] <= CI_limits[1]:
                color = GREEN
            else:
                color = RED
                
            if ci==True:
                line = Line(np.array([x[i], y[i], 0]), np.array([x[i+1], y[i+1], 0]), color=color)
            else:
                line = Line(np.array([x[i], y[i], 0]), np.array([x[i+1], y[i+1], 0]), color=BLUE)
            curve.add(line)
            
        # scale the curve to fit within the max_width
        curve.scale(max_width / curve.get_width())
        
        # add a line below the curve 
        x_line = Line(curve.get_left(), curve.get_right(), color=WHITE)
        
        x_line.next_to(curve, DOWN, buff=0.1)
        
        curve.add(x_line)
        
        return curve, x_line
    
    def get_voxel_position_from_object(self, obj, index_slice, voxel_pos):
        import warnings
        warnings.filterwarnings("ignore")
        local_voxel_position = voxel_pos
        
        # print the coordinates of the obj 
        center_obj = obj.get_center()
        ul_corner = obj.get_corner(UL)
        ur_corner = obj.get_corner(UR)
        ll_corner = obj.get_corner(DL)
        lr_corner = obj.get_corner(DR)
        
        x_i = local_voxel_position[0]
        y_i = local_voxel_position[1]
        
        width = 1.1851851851
        height = 1.1851851851
        
        x_UL = 3.5 - width / 2
        y_UL = width / 2
        
        
        
        x_rescale = (x_UL + x_i / 32 * width)
        y_rescale = (y_UL - y_i / 32 * height) 
        
        
        # x_position_rescale = local_voxel_position[1] / 32 * width
        # y_position_rescale = local_voxel_position[0] / 32 * height
        # #y_position_rescale += 0.2
        # #y_position_rescale *= 0.9
        # # convert the local voxel position to the global voxel position in terms of the object
        # #origin = obj.get_center()
        # origin_corner = obj.get_corner(UL)
        
        # x_position_voxel = origin_corner[0] + x_position_rescale
        # y_position_voxel = origin_corner[1] - y_position_rescale
        
        return np.array([x_rescale, y_rescale, 0])