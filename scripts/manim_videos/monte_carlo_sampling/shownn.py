from manim import *

def generate_unet(random_seed=0, full_network=True):
    import numpy as np
    np.random.seed(random_seed)  # Set random seed for reproducibility

    layers = [5, 4, 2, 4, 5]  # Number of nodes in each layer
    layer_spacing = 1.0  # Horizontal spacing between layers
    node_spacing = 1.0  # Vertical spacing between nodes within a layer
    node_radius = 0.2  # Radius of each node
    
    unet = VGroup()  # Create an empty VGroup to hold all the nodes and edges

    prev_layer_nodes = []  # Holds the nodes of the previous layer

    # Create nodes layer by layer
    for i, num_nodes in enumerate(layers):
        layer_nodes = []  # Holds the nodes of the current layer

        # Create a vertical line of nodes for the current layer
        for j in range(num_nodes):
            node = Circle(radius=node_radius, fill_opacity=1, fill_color=BLACK) # hide boundary
            node.set_stroke(color=BLUE, width=2.5) # show boundary
            node.move_to([i * layer_spacing, j * node_spacing - (num_nodes - 1) * node_spacing / 2, 0])
            layer_nodes.append(node)
            unet.add(node)

        # Create random edges between nodes of the previous layer and current layer
        if prev_layer_nodes:
            for prev_node in prev_layer_nodes:
                for cur_node in layer_nodes:
                    if full_network:
                        decide_edge = True
                    else:
                        if len(prev_layer_nodes) == 1 or len(prev_layer_nodes) == 3:
                            decide_edge = True
                        else:
                            decide_edge = np.random.choice([True, False], p=[0.8, 0.2])
                        
                    if decide_edge:  # Randomly decide whether to draw an edge
                        start, end = get_start_end_points(prev_node, cur_node, node_radius)
                        #width_random_distribution = np.random.normal(loc=2, scale=1)
                        edge = Line(start, end, color=WHITE, stroke_width=1.5)
                        unet.add(edge)

        prev_layer_nodes = layer_nodes

    return unet
    
def get_start_end_points(start_node, end_node, node_radius):
    start = start_node.get_center()
    end = end_node.get_center()

    # Calculate the unit vector from start to end
    unit_vector = (end - start) / np.linalg.norm(end - start)

    # Calculate the points on the boundaries of the nodes where the line should start and end
    start_point = start + unit_vector * node_radius
    end_point = end - unit_vector * node_radius

    return start_point, end_point

# Manim scene to visualize the U-Net architecture
class SimpleUNet(Scene):
    def construct(self):
        unet = generate_unet().move_to(ORIGIN)
        self.play(FadeIn(unet))
