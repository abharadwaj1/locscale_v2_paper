from manim import * 
import os 
import numpy as np
from emmer.ndimage.map_utils import load_map 
from shownn import generate_unet, get_start_end_points

class VoxelDistribution(Scene):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
        # define some variables
        
        # input paths 
        self.monte_carlo_cubes_dir = "/home/abharadwaj1/papers/elife_paper/figure_information/data/manim_videos/monte_carlo_samples/9112_cube_15"
        self.input_window_path = os.path.join(self.monte_carlo_cubes_dir, "input_map.mrc")
        self.locscale_window_path = os.path.join(self.monte_carlo_cubes_dir, "locscale_window.mrc")
        self.mean_path = os.path.join(self.monte_carlo_cubes_dir, "mean_map.mrc")
        self.var_path = os.path.join(self.monte_carlo_cubes_dir, "var_map.mrc")
        self.monte_carlo_samples_path = [os.path.join(self.monte_carlo_cubes_dir, "monte_carlo_sample_{}.mrc".format(i)) for i in range(15)]
        
        # load the maps
        self.monte_carlo_samples = [load_map(path)[0] for path in self.monte_carlo_samples_path]
        self.input_window = load_map(self.input_window_path)[0]
        self.mean_window = load_map(self.mean_path)[0]
        self.var_window = load_map(self.var_path)[0]
        
        # get slices and voxel values
        self.slice_index = 16 
        self.voxel_pos = np.array([12,22,self.slice_index])
        self.slice_indices_to_show = [2, 4, 8, 12, 14]
        
        self.central_slices = [sample[:,:,self.slice_index] for sample in self.monte_carlo_samples]
        self.input_window_slice = self.input_window[:,:,self.slice_index]
        self.voxel_intensities = [sample[self.voxel_pos[0], self.voxel_pos[1], self.voxel_pos[2]] for sample in self.monte_carlo_samples]
        self.mean_slice = self.mean_window[:,:,self.slice_index]
        self.var_slice = self.var_window[:,:,self.slice_index]
        
        # other variables
        self.corner_indices = []
        self.num_bins = 10
    
    def hide_all_mobjects(self):
        for mob in self.mobjects:
            mob.set_opacity(0)
    
    def show_all_mobjects(self):
        for mob in self.mobjects:
            mob.set_opacity(1)
    
    def show_title(self, title):
        # hide all mobjects
        self.hide_all_mobjects()
        # show the title
        title = Text(title, font_size=32).move_to(ORIGIN)
        self.play(Write(title))
        self.wait(2)
        # hide the title
        self.play(FadeOut(title))
        # show all mobjects
        self.show_all_mobjects()        
            
    def normalize(self, x):
        return (x - np.min(x)) / (np.max(x) - np.min(x)) * 255
            
    def construct(self):
        # Display the slices 
        title_text = Text("Visualizing Monte Carlo dropout", font_size=32).move_to(ORIGIN)
        self.play(Write(title_text))
        self.wait(2)
        self.play(FadeOut(title_text))
        
        # Display the input window
        input_window = ImageMobject(self.normalize(self.input_window_slice))
        input_window.scale(5)
        input_window.move_to(ORIGIN).shift(LEFT*4)
        self.play(FadeIn(input_window))
        
        # Display the neural network
        unet_monte_carlo = generate_unet(full_network=True).move_to(ORIGIN)
        self.play(FadeIn(unet_monte_carlo))
        self.wait(2)
        
        # Show the input window transformed by the neural network
        central_slice_1 = ImageMobject(self.normalize(self.central_slices[0]))
        central_slice_1.scale(5)
        central_slice_1.move_to(ORIGIN).shift(RIGHT * 4)
        self.play(Transform(input_window.copy(), central_slice_1))
        self.wait(2)
        self.play(FadeOut(central_slice_1))
        prev_unet = unet_monte_carlo
        prev_slice = central_slice_1
        # Repeat the above for the other slices
        for i in range(1, 15):
            text = Text("Sample # {}".format(i+1), font_size=32).move_to(ORIGIN).shift(UP * 3)
            unet_monte_carlo = generate_unet(full_network=False, random_seed=i*425).move_to(ORIGIN)
            central_slice_sample = ImageMobject(self.normalize(self.central_slices[i]))
            central_slice_sample.scale(5)
            central_slice_sample.move_to(ORIGIN).shift(RIGHT * 4)
            self.play(
                Write(text),
                FadeOut(prev_unet),
                FadeOut(prev_slice),
                FadeIn(unet_monte_carlo),
                FadeIn(central_slice_sample)
            )
            self.wait(0.5)
            self.play(FadeOut(text))
            prev_unet = unet_monte_carlo
            prev_slice = central_slice_sample
        
        # Fade off all the mobjects
        self.play(
            *[FadeOut(mob) for mob in self.mobjects]
        )
        
        # Display the slices
        slice_objects = self.display_slices()
        self.play(*[FadeIn(img) for img in slice_objects])
        self.wait(1)
        
        
        # # Display the voxel for the slices
        dot_objects = self.display_voxel_dots(slice_objects)
        self.play(*[FadeIn(dot) for dot in dot_objects])
        self.wait(1)
        
        
        # # Show the voxel intensities as a histogram
        histogram, bin_edge_text, line = self.display_histogram(self.voxel_intensities, bins=self.num_bins)
        histogram.move_to(ORIGIN)
        histogram.shift(UP * 2 + LEFT * 2)
        bin_edge_text.next_to(histogram, DOWN, buff=0.3)
        line.next_to(histogram, DOWN, buff=0.1)

        self.play(
            *[FadeIn(bar) for bar in histogram]
        )
        self.wait(2)
        
        # # Show the mean and variance of the voxel intensities
        # add a normal distribution curve to the histogram
        curve, mean_text, std_text = self.add_normal_distribution_curve(self.voxel_intensities, max_height=histogram.get_height()/25, max_width=histogram.get_width())
        curve.move_to(histogram.get_center())
        curve.shift(UP * 0.1)
        
        self.play(FadeIn(curve), FadeOut(histogram), FadeIn(mean_text), FadeIn(std_text))
        self.wait(2)
        
        # Transform the mean_text and std_text to mean_slice and var_slice
        mean_slice = ImageMobject(self.normalize(self.mean_slice))
        mean_slice.add(SurroundingRectangle(mean_slice, buff=0))
        mean_slice.scale(5)
        mean_slice.move_to(ORIGIN).shift(UP * 2 + RIGHT * 4)
        mean_slice_text = Text("Mean map", font_size=32).next_to(mean_slice, UP, buff=0.2)
        
        
        var_slice = ImageMobject(self.normalize(self.var_slice))
        var_slice.add(SurroundingRectangle(var_slice, buff=0))
        var_slice.scale(5)
        var_slice.next_to(mean_slice, DOWN, buff=1)
        var_slice_text = Text("Variance map", font_size=32).next_to(var_slice, UP, buff=0.2)
    
        
        self.play(
            Transform(mean_text, mean_slice_text),
            Transform(std_text, var_slice_text),
            FadeIn(mean_slice),
            FadeIn(var_slice),
        )
    
        self.wait(2)
        
        
        
    
    def display_slices(self, num_slices=4):
        # Display the slices 
        slice_objects = []
        for i, slice_index in enumerate(self.slice_indices_to_show):
            slice_object = ImageMobject(self.normalize(self.central_slices[slice_index]))
            slice_object.scale(5)
            
            # add a bounding box to the image
            slice_object.add(SurroundingRectangle(slice_object, buff=0))
            # add a text showing the slice index
            sample_num = i+1 if i < 4 else 15
            sample_index_text = Text("# {}".format(sample_num), font_size=22)
            slice_object.add(sample_index_text.next_to(slice_object, UP, buff=0.1))
            # move the slice object to the origin and shift down and to the left
            if len(slice_objects) > 0:
                if i == 4:
                    # add a text showing "..." to indicate that there are more slices
                    text = Text("...", font_size=32)
                    text.next_to(slice_objects[-1], RIGHT, buff=0.1)
                    slice_object.next_to(text, RIGHT, buff=0.1)
                    self.corner_indices.append(slice_object.get_corner(UL))
                    slice_object.add(text)
                    self.add(text)
                else:                    
                    slice_object.next_to(slice_objects[-1], RIGHT, buff=0.5)
                    self.corner_indices.append(slice_object.get_corner(UL))
            else:    
                slice_object.move_to(ORIGIN)
                slice_object.shift(DOWN * 2 + LEFT * 5)
                self.corner_indices.append(slice_object.get_corner(UL))
                
            slice_objects.append(slice_object)
            self.add(slice_object)
            
        
        return slice_objects
        
    def display_voxel_dots(self, slice_objects):
        # Display the voxel for the slices
        dot_objects = []
        for i, slice_object in enumerate(slice_objects):
            dot_object = Dot(color=RED).scale(0.5)
            dot_object.move_to(self.get_voxel_position_from_object(slice_object, index_slice=i))
            dot_objects.append(dot_object)
            self.add(dot_object)
        return dot_objects      
    
    def display_histogram(self, data, bins=10, max_height=3, bar_width=0.5, gap=0.2):
        """
        Display a histogram for the given data.

        Parameters:
        - data: Dataset for which the histogram is to be created.
        - bins: Number of bins for the histogram.
        - max_height: Maximum height for a bar in the scene.
        - bar_width: Width of each bar in the scene.
        - gap: Gap between consecutive bars.
        """
        from scipy.stats import norm
        
        # Compute histogram
        hist, bin_edges = np.histogram(data, bins=bins)

        # Scale the heights to fit within our max_height
        scaled_hist = max_height * hist / np.max(hist)

        # Create bars for the histogram
        bars = []
        for i, height in enumerate(scaled_hist):
            bar = Rectangle(width=bar_width, height=height)
            # Position the bars
            if len(bars) > 0:
                bar.next_to(bars[-1], RIGHT, buff=gap, aligned_edge=DOWN)
            else:
                bar.move_to(ORIGIN)
            bars.append(bar)
        
        # add a line below the bars 
        line = Line(bars[0].get_corner(DL), bars[-1].get_corner(DR))
        line.shift(DOWN * 0.1)
        self.add(line)
        # add a text showing the bin edges for each bar
        bin_edge_texts = []
        for i, bar in enumerate(bars):
            bin_edge_text = Text("{:.2f}".format(np.round(bin_edges[i], 2)), font_size=12)
            bin_edge_text.next_to(bar, DOWN, buff=0.3)
            # rotate the text by 45 degrees
            bin_edge_text.rotate(PI/4)
            bin_edge_texts.append(bin_edge_text)
            self.add(bin_edge_text)

        
        # convert the bars to a VGroup
        bars = VGroup(*bars)
        bin_edge_texts = VGroup(*bin_edge_texts)
        line = VGroup(line)
        
        return bars, bin_edge_texts, line

    def add_normal_distribution_curve(self, data, max_height=3, max_width=5):
        from scipy.stats import norm
        mean = np.mean(data)
        variance = np.var(data)
        std = np.sqrt(variance)
        x = np.linspace(mean - 3 * std, mean + 3 * std, 100) 
        y = norm.pdf(x, mean, std)
        y = max_height * y / np.max(y)
        curve = VGroup() # set color to RED
        curve.set_color(RED)
        
        for i in range(len(x) - 1):
            line = Line(np.array([x[i], y[i], 0]), np.array([x[i+1], y[i+1], 0]), color=RED)
            curve.add(line)
        
        # scale the curve to fit within the max_width
        curve.scale(max_width / curve.get_width())
        
        # Add a text showing the mean and variance
        mean_text = Text("Mean: {:.2f}".format(mean), font_size=18)
        mean_text.move_to(curve.get_corner(UR)).shift(UP * 1 + LEFT * 0.5)
        sigma_text = Text("Sigma: {:.2f}".format(std), font_size=18)
        sigma_text.next_to(mean_text, DOWN, buff=0.2)
        return curve, mean_text, sigma_text
    def get_voxel_position_from_object(self, obj, index_slice):
        local_voxel_position = self.voxel_pos 
        height = obj.get_height()
        width = obj.get_width()
        x_position_rescale = local_voxel_position[0] / 32 * width
        y_position_rescale = local_voxel_position[1] / 32 * height
        # convert the local voxel position to the global voxel position in terms of the object
        #origin = obj.get_center()
        origin_corner = self.corner_indices[index_slice]
        if index_slice == 4:
            origin_corner[0] -= 0.15
        x_position_voxel = origin_corner[0] + x_position_rescale
        y_position_voxel = origin_corner[1] - y_position_rescale
        
        return np.array([x_position_voxel, y_position_voxel, 0])