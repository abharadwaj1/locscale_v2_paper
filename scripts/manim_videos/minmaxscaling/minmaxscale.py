from manim import *
import numpy as np
np.random.seed(0)
class MinMaxScaling(Scene):
    def construct(self):
        # Generate random data points from an exponential distribution
        data_points = np.random.exponential(scale=7, size=20)
        min_val = np.min(data_points)
        max_val = np.max(data_points)

        # Create axis for original data
        original_axis = NumberLine(x_range=[0, 30], unit_size=0.2, numbers_with_elongated_ticks=[5, 10, 15, 20, 25, 30])
        original_axis_label = Text("Original Data", font_size=26).next_to(original_axis, DOWN)

        # Create points on original axis
        original_points = [Dot(point=[point * 0.2, 0, 0], color=BLUE) for point in data_points]
        original_point_group = VGroup(*original_points).next_to(original_axis, UP)

        # Indicate maximum and minimum values
        min_text = Text(f"Min: {min_val:.2f}", font_size=20).next_to(original_axis, LEFT)
        max_text = Text(f"Max: {max_val:.2f}", font_size=20).next_to(original_axis, RIGHT)

        # Create axis for scaled data
        scaled_axis = NumberLine(x_range=[0, 1], unit_size=5, numbers_with_elongated_ticks=[0, 0.2, 0.4, 0.6, 0.8, 1])
        scaled_axis_label = Text("Scaled Data", font_size=26).next_to(scaled_axis, DOWN)
        scaled_min_text = Text("Min: 0", font_size=20).next_to(scaled_axis, LEFT)
        scaled_max_text = Text("Max: 1", font_size=20).next_to(scaled_axis, RIGHT)

        # Calculate scaled points
        scaled_points = (data_points - min_val) / (max_val - min_val)

        # Create points on scaled axis
        scaled_point_group = VGroup(*[Dot(point=[point * 5, 0, 0], color=GREEN) for point in scaled_points]).next_to(scaled_axis, UP)

        # Add original axis and points to the scene
        self.play(Create(original_axis), Write(original_axis_label), Write(min_text), Write(max_text))
        self.wait(0.5)
        self.play( *[Create(dot) for dot in original_point_group],)
        

        # Move to the scaled axis while removing the "Original Data" label
        self.play(
            original_axis.animate.next_to(scaled_axis, UP, buff=1),
            FadeOut(original_axis_label),
            FadeOut(min_text),
            FadeOut(max_text)
        )

        self.wait(2)
        # Add scaled axis to the scene
        self.play(Create(scaled_axis), Write(scaled_axis_label), Write(scaled_min_text), Write(scaled_max_text))

        # Transform original points to scaled points
        
        self.play(Transform(original_point_group, scaled_point_group))

        self.wait(3)
