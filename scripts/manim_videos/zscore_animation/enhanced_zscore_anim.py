
from manim import *
import numpy as np
from scipy.stats import norm

class ZScoreAnimation(Scene):
    def construct(self):
        # Title
        title = Text("Z-Score Visualization", color=WHITE).to_edge(UP)
        self.play(Write(title))

        # Parameters for non-standard normal distribution
        mu = 0.2
        sigma = 0.04
        data = np.random.normal(mu, sigma, 1000)

        # Plot histogram
        hist = self.create_histogram(data)
        hist.to_edge(UP + LEFT, buff=0.5)
        self.play(FadeIn(hist))
        
        # Highlight point on histogram
        point_x = 0.14
        highlight_dot = self.highlight_point_on_hist(hist, point_x, data)
        self.play(FadeIn(highlight_dot))
        self.wait(1)

        # Calculate and display z-score
        z_score = (point_x - mu) / sigma
        z_score_text = Text(f"Z = \frac{{x - \mu}}{{\sigma}} = {z_score:.2f}").next_to(hist, DOWN)
        self.play(Write(z_score_text))
        self.wait(2)

        # Plotting the chart for Probability vs X
        prob_vs_x_chart = self.plot_probability_vs_x(mu, sigma)
        self.play(FadeIn(prob_vs_x_chart))
        self.wait(3)


    def create_histogram(self, data):
        freq, bins = np.histogram(data, bins=30)
        bin_width = (bins[1] - bins[0]) * 8
        max_height = max(freq)
        bars = VGroup(*[
            Rectangle(width=bin_width, height=(f/max_height), fill_opacity=0.8, fill_color=BLUE, stroke_opacity=0)
            for f in freq
        ])
        for i, bar in enumerate(bars):
            bar.move_to(i * RIGHT * bin_width)
        bars.move_to(ORIGIN)
        return bars

    def highlight_point_on_hist(self, bars, x_value, data):
        freq, bins = np.histogram(data, bins=30)
        width = bars.get_width()
        xmin = bins[0]
        xmax = bins[-1]
        origin = bars.get_left()[0]
        relative_position = (x_value - xmin) / (xmax - xmin)
        x_value_rescaled = origin + relative_position * width
        dot = Dot(point=[x_value_rescaled, 0, 0], color=RED)
        return dot


    def plot_probability_vs_x(self, mu, sigma):
        # Generate x values spanning the range of interest
        x_vals = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
        
        # Calculate the corresponding rescaled probabilities for each x
        z_scores = (x_vals - mu) / sigma
        probabilities = norm.cdf(z_scores) * 200 - 100

        # Create a graph of rescaled probability vs. x
        graph = Axes(
            x_range=[mu - 3*sigma, mu + 3*sigma],
            y_range=[-100, 100],           
        )

        graph_label_x = Text("X").set_color(WHITE).next_to(graph.x_axis, RIGHT)
        graph_label_y = Text("Rescaled Probability").set_color(WHITE).next_to(graph.y_axis, UP)
        graph_curve = graph.get_graph(lambda x: (norm.cdf((x - mu) / sigma) * 200 - 100))

        graph.add(graph_curve, graph_label_x, graph_label_y)
        graph.to_corner(DOWN + RIGHT)

        return graph

