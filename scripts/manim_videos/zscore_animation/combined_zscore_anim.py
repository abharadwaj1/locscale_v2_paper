from manim import *
import numpy as np
from scipy.stats import norm, gaussian_kde

class ZScoreAnimation(Scene):
    def construct(self):

        self.animate_this()
            

    def animate_this(self):
        # Parameters for non-standard normal distribution
        mu = 0.2
        sigma = 0.04
        data = np.random.normal(mu, sigma, 1000)

        # Plot histogram for the non-standard normal distribution (with larger height)
        hist = self.create_histogram(data, scale_factor=2.0)  # Increase the scale_factor for larger bars
        # move slightly to the right
        hist.to_edge(UP + RIGHT, buff = 0.5)
        self.play(Write(hist))
        
        # Highlight the point x=0.25 on the histogram
        point_x = 0.14 #np.random.choice(data)
        point_x = np.round(point_x, 2)
        highlight_line = self.highlight_point_on_hist(hist, point_x, data)
        self.play(Write(highlight_line))
        self.wait(2)

        # Plotting the chart for Probability vs X
        prob_vs_x_chart = self.plot_probability_vs_x(mu, sigma)
        self.play(FadeIn(prob_vs_x_chart))
        self.wait(3)

        
        # Calculate and display z-score
        x = point_x
        z_score = (x - mu) / sigma
        x_text = Text(f"x = {x:.2f}", font_size=20).next_to(hist, DOWN)
        #mean_text = Text(f"mean = {mu:.2f}", font_size=20).next_to(x_text, DOWN)
        #std_text = Text(f"std = {sigma:.2f}", font_size=20).next_to(mean_text, DOWN)
        self.play(Write(x_text))
        self.wait(1)

        # Transition to KDE plot for standard normal distribution
        #self.play(FadeOut(hist), FadeOut(line), FadeOut(z_text))
        
        # Plot KDE for standard normal distribution
        kde_graph = self.create_normal_distribution()
        kde_graph.to_edge(UP + LEFT, buff = 0.5)
        self.play(Write(kde_graph))       
        
        # Highlight the z-score on the KDE plot
        z_line, intersection_area = self.highlight_point_on_kde(kde_graph, z_score)
        self.play(Write(z_line))
        self.wait(2)
        
        # display z-score
        z_text = Text(f"z = {z_score:.2f}").next_to(kde_graph, DOWN)
        self.play(Write(z_text))
        self.wait(1)
        
        probability = norm.cdf(z_score)
        # display probability
        prob_text = Text(f"probability = {probability:.2f}").next_to(z_text, DOWN)
        self.play(Write(prob_text))
        self.wait(1)
        
        # Create a rectangle with the same area
        rect_height = kde_graph.get_height() * 2
        rect_width_total = kde_graph.get_width() / 2
        rect_width_prob = rect_width_total * probability
        
        rectangle_total = Rectangle(width=rect_width_total, height=rect_height, fill_color=BLUE, fill_opacity=0.5, stroke_opacity=0)
        rectangle_total.align_to(kde_graph, DOWN).next_to(kde_graph, DOWN, buff=2)
        rectangle_prob = Rectangle(width=rect_width_prob, height=rect_height, fill_color=RED, fill_opacity=0.8, stroke_opacity=0)
        rectangle_prob.align_to(kde_graph, DOWN).next_to(kde_graph, DOWN, buff=2)
        
        # Transition to the rectangle
        self.play(ReplacementTransform(kde_graph.copy(), rectangle_total), ReplacementTransform(intersection_area.copy(), rectangle_prob))
        self.wait(2)
        
        
    def create_histogram(self, data, scale_factor=1.0):
        freq, bins = np.histogram(data, bins=30)
        bin_width = (bins[1] - bins[0])*8

        max_height = max(freq)
        bars = VGroup(*[
            Rectangle(width=bin_width, height=(f/max_height)*scale_factor, fill_opacity=0.8, fill_color=BLUE, stroke_opacity=0)  # Making the edge transparent
            for f in freq
        ])

        # Position bars next to each other
        for i, bar in enumerate(bars):
            bar.move_to(i * RIGHT * bin_width)

            # Create x-axis
        x_labels_left = Text(f"{min(data):.2f}", font_size=20).next_to(bars[0], LEFT)
        x_labels_right = Text(f"{max(data):.2f}", font_size=20).next_to(bars[-1], RIGHT)
        
        # plot the mean and standard deviation
        mu = np.mean(data)
        sigma = np.std(data)
        
        mean_line = self.create_vertical_line(bars, label = "mean={:.2f}".format(mu))
        std_line = self.create_horizontal_line(mu, sigma, bars, label = "std = {:.2f}".format(sigma), bins = bins)
        
        return VGroup(bars, x_labels_left, x_labels_right, mean_line, std_line)

    def create_vertical_line(self, bars, label):
        # Create a vertical line at the mean
        x_pos = bars.get_center()[0] 
        
        # Create a vertical line at that position to highlight
        line = DashedLine(
            start=[x_pos, bars.get_bottom()[1], 0],
            end=[x_pos, bars.get_top()[1], 0],
            color=RED
        )
        
        text = Text(label, font_size=20).next_to(line, UP)
        
        return VGroup(line, text)
    
    def create_horizontal_line(self, mu, sigma, bars, label, bins):
        # create a horizontal line 
        x_pos = bars.get_center()[0]
        # convert sigma to the same scale as the histogram
        width = bars.get_width() 
        xmin = bins[0]
        xmax = bins[-1]
        origin = bars.get_center()[0]
        # rescale sigma
        sigma_rescale = sigma * width / (xmax - xmin)
        # create the line
        line = Line(
            start=[origin - sigma_rescale, 0, 0],
            end = [origin + sigma_rescale, 0, 0],
            color=BLACK
        )
        
        text = Text(label, font_size=20).next_to(line, UP)
        
        return VGroup(line, text)
        

    def create_normal_distribution(self):
        x = np.linspace(-3, 3, 400)
        y = norm.pdf(x) * 4  # Scale the y-axis to match the histogram

        # Convert x, y data to a manim graph
        points = [np.array([xi, yi, 0]) for xi, yi in zip(x, y)]
        graph = VMobject()
        graph.set_points_smoothly(points)
        graph.set_color(BLUE)
        return graph

    def highlight_point_on_hist(self, bars, x_value, data):        
        freq, bins = np.histogram(data, bins=30)
        bin_width = (bins[1] - bins[0])*8
        
        width = bars.get_width() 
        xmin = min(bins)
        xmax = max(bins)
        origin = bars.get_center()[0]
        y_val_origin = bars.get_center()[1]
        mean_value = np.mean(data)
        x_value_rescale = x_value * width / (xmax - xmin)
        x_value_from_left_end = bars.get_left()[0] + x_value_rescale
        # Create a dot at the x_value position
        dot = Dot(point=[x_value_from_left_end, y_val_origin, 0], color=RED, radius=0.1)
        
        return dot

    
    def highlight_point_on_kde(self, kde_graph, z_score):
        # Determine the position for the z_score on the KDE plot
        x_pos = kde_graph.get_left()[0] + z_score * kde_graph.get_width() / 2
        
        # Create a vertical line at that position to highlight
        line = DashedLine(
            start=[x_pos, kde_graph.get_bottom()[1], 0],
            end=[x_pos, kde_graph.get_top()[1], 0],
            color=RED
        )

        # Create a large rectangle covering the region to the left of the z-score line
        rectangle = Rectangle(
            width=abs(kde_graph.get_left()[0] - x_pos),
            height=kde_graph.get_height(),
            fill_opacity=0,
            stroke_opacity=0
        )
        rectangle.next_to(line, LEFT, buff=0)
        rectangle.align_to(kde_graph, DOWN)

        # Get the intersection of the KDE graph and the rectangle
        intersection_area = Intersection(kde_graph, rectangle).set_fill(RED, opacity=0.6).set_stroke(RED, opacity=0)

        return VGroup(line, intersection_area), intersection_area
    
    def plot_probability_vs_x(self, mu, sigma):
        # Generate x values spanning the range of interest
        x_vals = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
        
        # Calculate the corresponding rescaled probabilities for each x
        z_scores = (x_vals - mu) / sigma
        probabilities = norm.cdf(z_scores) * 200 - 100

        # Create a graph of rescaled probability vs. x
        x = x_vals
        y = probabilities
        points = [np.array([xi, yi, 0]) for xi, yi in zip(x, y)]
        graph = VMobject()
        graph.set_points_smoothly(points)
        graph.set_color(BLUE)
        graph.to_corner(DOWN + RIGHT)

        return graph


