from manim import *
import numpy as np

class CentralLimitTheorem(Scene):
    def construct(self):
        # Parameters
        sample_sizes = [5, 50, 500]
        population_size = 1000
        samples_to_take = 100

        # Create a non-normally distributed population (e.g., uniform distribution)
        population = np.random.uniform(0, 10, population_size)

        # Title
        title = Text("Central Limit Theorem Illustration")
        self.play(Write(title))
        self.wait(1)
        self.play(FadeOut(title))

        for sample_size in sample_sizes:
            # Take multiple samples and compute their means
            sample_means = [np.mean(np.random.choice(population, sample_size)) for _ in range(samples_to_take)]
            
            # Plot histogram of sample means
            hist = self.create_histogram(sample_means)
            label = Text(f"Sample size: {sample_size}").next_to(hist, UP)
            self.play(Write(hist), Write(label))
            self.wait(2)
            self.play(FadeOut(hist), FadeOut(label))

    def create_histogram(self, data):
        # Create a histogram from data and convert to a Manim object
        freq, bins = np.histogram(data, bins=10)
        bin_width = bins[1] - bins[0]
        
        max_height = max(freq)
        bars = VGroup(*[
            Rectangle(width=bin_width, height=f/max_height, fill_opacity=0.8, fill_color=YELLOW)
            for f in freq
        ])
        
        # Position bars next to each other
        for i, bar in enumerate(bars):
            bar.move_to(i * RIGHT * bin_width)
        
        return bars
