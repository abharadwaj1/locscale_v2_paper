from manim import *
import numpy as np
from scipy.stats import norm

class SimpleNormalCurve(Scene):
    def construct(self):
        # Step 1: Plot standard normal curve on the left
        curve = ParametricFunction(
            lambda t: np.array([t, norm.pdf(t) * 10, 0]),
            t_range=[-4, 4]
        )
        curve.to_edge(LEFT, buff=2)
        self.play(Create(curve))

        # Step 2: Indicate the z-score with a vertical line
        z = -0.75
        z_line = Line([z, 0, 0], [z, norm.pdf(z) * 10, 0], color=RED)
        z_line.next_to(curve, DOWN, aligned_edge=LEFT)
        self.play(Create(z_line))

        # Step 3: Shade the area under the curve to the left of z
        area = ParametricFunction(
            lambda t: np.array([t, norm.pdf(t) * 10, 0]),
            t_range=[-4, z],
            fill_opacity=0.5,
            fill_color=BLUE,
            color=BLUE
        )
        area.next_to(curve, DOWN, aligned_edge=LEFT)
        self.play(Create(area))

        # Step 4: Display the probability on the right
        cdf_value = norm.cdf(z)
        probability_text = Text(f"Probability = {cdf_value * 100:.2f}%").to_edge(RIGHT, buff=2)
        self.play(Write(probability_text))

        self.wait()
