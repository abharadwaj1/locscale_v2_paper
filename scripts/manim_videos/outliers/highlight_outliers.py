from manim import *
import numpy as np

class HighlightOutliers(Scene):
    def construct(self):
        # Generate data from T-distribution with low degrees of freedom
        data = np.random.standard_t(df=15, size=1000)
        # Plot histogram with highlighted outliers
        hist = self.create_histogram(data)
        
        # center histogram
        hist.move_to(ORIGIN)
        
        self.play(Write(hist))
        self.wait(3)

    def create_histogram(self, data):
        freq, bins = np.histogram(data, bins=30)
        # outliers if data is outside 1 standard deviation from mean
        
        bin_width = bins[1] - bins[0]

        max_height = max(freq)
        lower_bound = np.mean(data) - np.std(data)
        upper_bound = np.mean(data) + np.std(data)
        outliers = [True if x < lower_bound or x > upper_bound else False for x in bins[:-1]]
        bars = VGroup(*[
            Rectangle(width=bin_width, height=f/max_height, fill_opacity=0.8, 
                    fill_color=RED if outliers[i] else BLUE) 
            for i, f in enumerate(freq)
        ])

        # Position bars next to each other
        for i, bar in enumerate(bars):
            bar.move_to(i * RIGHT * bin_width)

        return bars
