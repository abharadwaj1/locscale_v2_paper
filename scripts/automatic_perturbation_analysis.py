# Script to perturb and refine a given structure

import os
import numpy as np
from locscale.include.emmer.pdb.pdb_utils import shake_pdb_within_mask


from genericpath import isfile
import os
import sys
import shutil
import subprocess
import numpy as np
from datetime import datetime
from joblib import Parallel, delayed
import gemmi
import math
import random

def copy_files_to_folder(file, folder):
    # if copied file already exists then ignore 
    test_copied_path = os.path.join(folder, os.path.basename(file))
    if os.path.exists(test_copied_path):
        print("File already exists: {}".format(test_copied_path))
        return test_copied_path
    
    if not os.path.exists(folder):
        os.makedirs(folder)
    new_path = shutil.copy(file, folder)
    return new_path

def replace_atoms_with_pseudo_atoms(atomic_model_path):
    st = gemmi.read_structure(atomic_model_path)
    
    for cra in st[0].all():
        cra.atom.name = "O"
        cra.atom.element = gemmi.Element("O")
        cra.residue.name = "HOH"
    
    return st

def remove_atoms(st, num_atoms_to_remove):
    st_local = st.clone()
    num_atoms_total = st_local[0].count_atom_sites()
    assert num_atoms_to_remove < num_atoms_total, "Number of atoms to remove is greater than total number of atoms"

    probability_of_removing_atom = num_atoms_to_remove/num_atoms_total

    num_atoms_removed = 0
    while num_atoms_removed < num_atoms_to_remove:
        for chain in st_local[0]:
            for res in chain:
                for atom in res:
                    if num_atoms_removed >= num_atoms_to_remove:
                        break
                    atom_survival_chance = np.random.uniform()
                    if atom_survival_chance < probability_of_removing_atom:
                        #print("Removing atom: {}, {}, {}".format(atom.name, atom.altloc, atom.element))
                        try:
                            res.remove_atom(name=atom.name, altloc=atom.altloc, el=atom.element)
                            num_atoms_removed += 1
                        except:
                            print("Failed to remove atom: {}, {}, {}".format(atom.name, atom.altloc, atom.element))
            

    print("Number of atoms after removal: {}".format(st_local[0].count_atom_sites()))
    return st_local

def random_point_on_sphere(x, y, z, d):
    phi = math.acos(2 * random.random() - 1)
    theta = 2 * math.pi * random.random()

    x1 = x + d * math.sin(phi) * math.cos(theta)
    y1 = y + d * math.sin(phi) * math.sin(theta)
    z1 = z + d * math.cos(phi)

    return x1, y1, z1
    
def add_atoms(st, num_atoms_to_add, min_dist):
    st_local = st.clone()
    num_atoms_total = st_local[0].count_atom_sites()
    assert num_atoms_to_add > 0, "Number of atoms to add is less than 0"

    probability_of_adding_atom = num_atoms_to_add/num_atoms_total
    num_atoms_added = 0
    while num_atoms_added < num_atoms_to_add:
        for chain in st_local[0]:
            for res in chain:
                for atom in res:
                    if num_atoms_added >= num_atoms_to_add:
                        break
                    
                    atom_add_chance = np.random.uniform()
                    if atom_add_chance < probability_of_adding_atom:
                        atom_pos = np.array([atom.pos.x, atom.pos.y, atom.pos.z])
                        new_atom_pos = random_point_on_sphere(atom_pos[0], atom_pos[1], atom_pos[2], min_dist)
                        new_atom = gemmi.Atom()
                        new_atom.name = "O"
                        new_atom.element = gemmi.Element("O")
                        new_atom.pos = gemmi.Position(new_atom_pos[0], new_atom_pos[1], new_atom_pos[2])
                        res.add_atom(new_atom)    
                        num_atoms_added += 1
    return st_local

def modify_pdb_num_atoms(pdb_path, num_atoms_frac,perturb_magnitude):
    st = gemmi.read_structure(pdb_path)
    num_atoms = st[0].count_atom_sites()
    if num_atoms_frac < 1:
        # remove atoms
        num_atoms_to_remove = int(num_atoms*(1-num_atoms_frac))
        print("Removing {} atoms".format(num_atoms_to_remove))
        st_removed_atoms = remove_atoms(st, num_atoms_to_remove)
        return st_removed_atoms 
    elif num_atoms_frac > 1:
        # add atoms
        num_atoms_to_add = int(num_atoms*(num_atoms_frac-1))
        print("Adding {} atoms".format(num_atoms_to_add))
        st_added_atoms = add_atoms(st, num_atoms_to_add, min_dist=perturb_magnitude)
        return st_added_atoms
    else:
        return st
    
def get_average_coordinate_error(atomic_model, pseudo_atomic_model):
    """Get the average coordinate error between the atomic model and the pseudo-atomic model"""
    coordinate_error = []
    # loop over each atom in the pseudo-atomic model and find the nearest atom in the atomic model
    # coordinate error is the distance between the two atoms
    st_pseudo = gemmi.read_structure(pseudo_atomic_model)
    st_atomic = gemmi.read_structure(atomic_model)

    ns_pseudo = gemmi.NeighborSearch(st_pseudo[0], st_pseudo.cell, 10).populate()
    ns_atomic = gemmi.NeighborSearch(st_atomic[0], st_atomic.cell, 10).populate()

    for cra_pseudo in st_pseudo[0].all():
        atom_pseudo = cra_pseudo.atom
        nearest_search = ns_atomic.find_nearest_atom(atom_pseudo.pos)
        if nearest_search is None:
            continue
        atom_atomic = nearest_search.to_cra(st_atomic[0]).atom
        coordinate_error.append(atom_pseudo.pos.dist(atom_atomic.pos))
    
    coordinate_error = np.array(coordinate_error)
    return coordinate_error

input_unsharpened_map = sys.argv[1]
input_refined_pdb = sys.argv[2]
print(sys.argv)
print(input_unsharpened_map)
print(input_refined_pdb)
input_unsharpened_map = os.path.abspath(input_unsharpened_map)
input_refined_pdb = os.path.abspath(input_refined_pdb)

assert os.path.exists(input_unsharpened_map), "Unsharpened map does not exist: {}".format(input_unsharpened_map)
assert os.path.exists(input_refined_pdb), "Refined pdb does not exist: {}".format(input_refined_pdb)

emmap_basename = os.path.basename(input_unsharpened_map).split(".")[0]
JOB_ID = emmap_basename

num_atoms_refinement_folder = "/home/abharadwaj1/dev/current_focus/study_effect_of_num_atoms/num_atoms_study_{}".format(JOB_ID)
if not os.path.exists(num_atoms_refinement_folder):
    os.makedirs(num_atoms_refinement_folder)

emmap_path_local = copy_files_to_folder(input_unsharpened_map, num_atoms_refinement_folder)
refined_pdb_path_local = copy_files_to_folder(input_refined_pdb, num_atoms_refinement_folder)
atomic_model_to_pseudomodel = replace_atoms_with_pseudo_atoms(refined_pdb_path_local)
atomic_model_to_pseudomodel_path = os.path.join(num_atoms_refinement_folder, "atomic_model_to_pseudomodel.pdb")
atomic_model_to_pseudomodel.write_pdb(atomic_model_to_pseudomodel_path)

# Get atomic model mask
from locscale.include.emmer.ndimage.map_tools import get_atomic_model_mask
atomic_model_mask_path = os.path.join(num_atoms_refinement_folder, "atomic_model_mask.mrc")
atomic_model_mask_path = get_atomic_model_mask(emmap_path=emmap_path_local, pdb_path=refined_pdb_path_local, \
                                               output_filename = atomic_model_mask_path, save_files=True)

# %%
from locscale.include.emmer.ndimage.map_utils import load_map
from locscale.preprocessing.headers import run_servalcat_iterative
refinement_paths_input = {}
emmap, apix = load_map(emmap_path_local)
num_atoms_fraction = [0.6, 0.8, 1, 1.2, 1.4]

for num_atoms_frac in num_atoms_fraction:
    num_atoms_folder = os.path.join(num_atoms_refinement_folder, "num_atoms_frac_{}".format(int(num_atoms_frac*100)))
    if not os.path.exists(num_atoms_folder):
        os.makedirs(num_atoms_folder)
    perturb_magnitude = 2
    perturbed_pdb_path = os.path.join(num_atoms_folder, "perturbed_pdb_rmsd_{}A.pdb".format(perturb_magnitude))
    perturbed_pdb = shake_pdb_within_mask(pdb_path = atomic_model_to_pseudomodel, mask_path = atomic_model_mask_path, \
                                          rmsd_magnitude = perturb_magnitude, use_pdb_mask = False)
    perturbed_pdb.write_pdb(perturbed_pdb_path)

    adjusted_pdb_path = os.path.join(num_atoms_folder, "adjusted_pdb_frac_{}.pdb".format(int(num_atoms_frac*100)))
    adjusted_pdb = modify_pdb_num_atoms(pdb_path = perturbed_pdb_path, num_atoms_frac = num_atoms_frac, perturb_magnitude = perturb_magnitude)
    adjusted_pdb.write_pdb(adjusted_pdb_path)
    
    # Print the average coordinate error
    error=get_average_coordinate_error(atomic_model = atomic_model_to_pseudomodel_path, pseudo_atomic_model = adjusted_pdb_path)
    mean_error = np.mean(error)
    print("Average coordinate error for {}\% atoms is {:.2f}".format(int(num_atoms_frac*100), mean_error))

    copied_emmap_path = copy_files_to_folder(emmap_path_local, num_atoms_folder)

    refinement_paths_input[num_atoms_frac] = {
        "model_path": adjusted_pdb_path,
        "map_path": copied_emmap_path,
        "resolution": round(apix*2),
        "num_iter": 10, 
        "pseudomodel_refinement" : True, 
    }

# Atomic model refinement
atomic_model_directory = os.path.join(num_atoms_refinement_folder, "atomic_model_refinement")
if not os.path.exists(atomic_model_directory):
    os.makedirs(atomic_model_directory)

copied_emmap_path = copy_files_to_folder(emmap_path_local, atomic_model_directory)
copied_pdb_path = copy_files_to_folder(refined_pdb_path_local, atomic_model_directory)

refinement_paths_input["atomic_model"] = {
    "model_path": copied_pdb_path,
    "map_path": copied_emmap_path,
    "resolution": round(apix*2),
    "num_iter": 10,
    "pseudomodel_refinement" : False,
}


# %%
from joblib import Parallel, delayed

refined_results = Parallel(n_jobs=11)(delayed(run_servalcat_iterative)(
    model_path = refinement_paths_input[key]["model_path"],            
    map_path = refinement_paths_input[key]["map_path"],
    resolution = refinement_paths_input[key]["resolution"],
    num_iter = refinement_paths_input[key]["num_iter"],
    pseudomodel_refinement = refinement_paths_input[key]["pseudomodel_refinement"],
)
for key in list(refinement_paths_input.keys()))

# %%







