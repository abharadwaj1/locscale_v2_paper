# %%
# Script to test how robust Emmernet is to input sharpening
import os
import numpy as np
import matplotlib.pyplot as plt

main_folder="/home/abharadwaj1/shortcuts/locscale_mapdata/dataset_attempt_EPSILON_HYBRID_MODEL_4_EPOCH_15_GPU1/0560_6nzu/input_sharpened_analysis/sharpening_analysis_again"

unsharpened_emmap_path = os.path.join(main_folder, "EMD_560_unsharpened_fullmap.mrc")
output_folder = os.path.join(main_folder, "outputs_hybrid_emmernet")
if not os.path.exists(output_folder):
    os.mkdir(output_folder)

# %%
# Create a bunch of globally sharpened maps
from locscale.preprocessing.headers import prepare_sharpen_map

global_bfactor_target_list = [-100, -50,  0, 50, 100] 
target_sharpened_map_paths = [unsharpened_emmap_path.replace(".mrc", "_global_bfactor_{}.mrc".format(bfactor)) for bfactor in global_bfactor_target_list]

for target_sharpened_map_path, bfactor in zip(target_sharpened_map_paths, global_bfactor_target_list):
    if os.path.exists(target_sharpened_map_path):
        continue
    else:
        outputpath = prepare_sharpen_map(emmap_path=unsharpened_emmap_path, wilson_cutoff=9, fsc_resolution=3.2, \
                            target_bfactor=bfactor, output_file_path=target_sharpened_map_path)
        if outputpath != target_sharpened_map_path:
            raise ValueError("Output path mismatch")
        
        if not os.path.exists(outputpath):
            raise ValueError("Output path does not exist")


# %%

# create a Emmernet command 
jobs = []
target_map_path = "/home/abharadwaj1/shortcuts/locscale_mapdata/dataset_attempt_EPSILON_HYBRID_MODEL_4_EPOCH_15_GPU1/0560_6nzu/input_sharpened_analysis/emd_0560_model_based_locscale_epsilon_MB.mrc"
assert os.path.exists(target_map_path)
for i, sharpened_map in enumerate(target_sharpened_map_paths):
    EM_outfile = os.path.join(output_folder, f"emd_0560_emmernet_hybrid_locscale_bfactor_input_{global_bfactor_target_list[i]}.mrc")
    EM_output_processing_folder = os.path.join(output_folder, "emd_0560_emmernet_hybrid_locscale_bfactor_input_{}".format(global_bfactor_target_list[i]))

    emmernet_command = []
    emmernet_command.append("python")
    emmernet_command.append("/home/abharadwaj1/dev/locscale/locscale/main.py")
    emmernet_command.append("run_emmernet")
    emmernet_command.append("--emmap_path")
    emmernet_command.append(sharpened_map)
    emmernet_command.append("--verbose")
    emmernet_command.append("--outfile")
    emmernet_command.append(EM_outfile)
    emmernet_command.append("-op")
    emmernet_command.append(EM_output_processing_folder)
    emmernet_command.append("--gpu_ids")
    emmernet_command.append("5")
    emmernet_command.append("--trained_model")
    emmernet_command.append("hybrid")
    emmernet_command.append("--target_map_path")
    emmernet_command.append(target_map_path)
    jobs.append(emmernet_command)

emmernet_unsharp_command = []
emmernet_unsharp_command.append("python")
emmernet_unsharp_command.append("/home/abharadwaj1/dev/locscale/locscale/main.py")
emmernet_unsharp_command.append("run_emmernet")
emmernet_unsharp_command.append("--emmap_path")
emmernet_unsharp_command.append(unsharpened_emmap_path)
emmernet_unsharp_command.append("--verbose")
emmernet_unsharp_command.append("--outfile")
emmernet_unsharp_command.append(os.path.join(output_folder, "emd_3061_emmernet_hybrid_locscale_bfactor_input_unsharpened.mrc"))
emmernet_unsharp_command.append("-op")
emmernet_unsharp_command.append(os.path.join(output_folder, "emd_3061_emmernet_hybrid_locscale_bfactor_input_unsharpened"))
emmernet_unsharp_command.append("--gpu_ids")
emmernet_unsharp_command.append("5")
emmernet_unsharp_command.append("--trained_model")
emmernet_unsharp_command.append("hybrid")
emmernet_unsharp_command.append("--target_map_path")
emmernet_unsharp_command.append(target_map_path)
jobs.append(emmernet_unsharp_command)



# %%
def run_emmernet(job):
    import subprocess
    
    try:
        print("Now running : \n{}".format(" ".join(job)))
        subprocess.run(job)
    except subprocess.TimeoutExpired as e:
        print("TimeoutExpired")
        print(e)
        return 2
    except subprocess.CalledProcessError as e:
        print("CalledProcessError")
        print(e)
        return 3
    except Exception as e:
        print("Exception")
        print(e)
        return 4
    
    return 0

# %%
# Run Emmernet
os.environ["CUDA_VISIBLE_DEVICES"] = "5"
for job in jobs:
    run_emmernet(job)


# %%



