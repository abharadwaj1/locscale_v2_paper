import os 
import shutil 
import numpy as np
import pandas as pd
import subprocess
import sys 

########################################################################
# Arguments
########################################################################
emmap_path = sys.argv[1]
emmap_path = os.path.abspath(emmap_path)
assert os.path.exists(emmap_path)

GPUS = sys.argv[2]
emmernet_model_paths = sys.argv[3:]
emmernet_model_paths = [model_path for model_path in emmernet_model_paths if os.path.exists(model_path)]

assert len(GPUS) > 0

########################################################################
# Define functions
########################################################################
def create_emmernet_cmd(input_files):
    cmd = []
    cmd.append('python')
    locscale_path = os.environ['LOCSCALE_PATH']
    assert os.path.exists(locscale_path)
    cmd.append(locscale_path)
    cmd.append('run_emmernet')
    cmd.append('-em')
    cmd.append(input_files['unsharpened_map'])
    cmd.append('-o')
    cmd.append(input_files['output_filename'])
    cmd.append('-gpus')
    cmd.append(input_files['gpus'])
    cmd.append('-v')
    cmd.append('--model_path')
    cmd.append(input_files['model_path'])
    cmd.append('--monte_carlo')
    print(cmd)
    return cmd

def run_emmernet(cmd):
    print('Running command: \n')
    print(' '.join(cmd))
    try:
        subprocess.run(cmd, check=True)
    except subprocess.CalledProcessError as e:
        print('Error running command: {}'.format(' '.join(cmd)))
        print(e)
        raise e
    print("."*80)


########################################################################

# create a output folder for each model_path used 
parent_output_dir = os.path.join(os.path.dirname(emmap_path), "study_different_models")

if not os.path.exists(parent_output_dir):
    os.makedirs(parent_output_dir)

model_path_local = {}
for i, model_path in enumerate(emmernet_model_paths):
    output_folder = os.path.join(parent_output_dir, f"model_{i}")
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    
    # save the model path in the output folder
    local_model_path = shutil.copy(model_path, output_folder)
    model_path_local[i] = local_model_path

# Copy unsharpened maps from test EMDB id into output folder


input_files = {}
for i in model_path_local:
    model_path = model_path_local[i]

    output_folder = os.path.dirname(model_path)

    # Copy unsharpened maps
    unsharpened_map = shutil.copy(emmap_path, output_folder)
    # Output filename
    extension = os.path.splitext(emmap_path)[1]
    output_filename = os.path.join(output_folder, os.path.basename(emmap_path).replace(extension, f"_model_{i}_output.mrc"))
    gpus = " ".join(GPUS) 
    input_files[i] = {
        'unsharpened_map': unsharpened_map,
        'output_filename': output_filename,
        'gpus': gpus,
        'model_path': model_path
    }
    
emmernet_commands = [create_emmernet_cmd(input_files[i]) for i in model_path_local]

for cmd in emmernet_commands:
    run_emmernet(cmd)

print('Finished running emmernet on all inputs')

