import os
import subprocess
import argparse
import re

def get_emdb_pdb_pairs(ref_dirs):
    """
    Collects all unique emdb_pdb pairs from the subfolders in the reference directories.
    """
    emdb_pdb_pairs = set()
    for ref_dir in ref_dirs:
        for subfolder in os.listdir(ref_dir):
            subfolder_path = os.path.join(ref_dir, subfolder)
            if os.path.isdir(subfolder_path):
                # Extract EMDB ID and PDB ID from subfolder name (format: 'EMDBID_PDBID')
                match = re.match(r'(\d{4})_(\w+)', subfolder)
                if match:
                    emdb_id_padded = match.group(1)
                    pdb_id = match.group(2)
                    emdb_pdb_pairs.add((emdb_id_padded, pdb_id))
    return emdb_pdb_pairs

def main():
    parser = argparse.ArgumentParser(description='Run locscale on multiple datasets with different references.')
    parser.add_argument('--dry-run', action='store_true', help='Perform a dry run without executing commands.')
    args = parser.parse_args()

    # Paths to data directories
    unsharpened_maps_dir = '/home/abharadwaj1/papers/elife_paper/figure_information/data/unsharpened_maps'
    masks_dir = '/home/abharadwaj1/papers/elife_paper/figure_information/data/confidence_masks'
    deepemhancer_dir = '/home/abharadwaj1/papers/elife_paper/figure_information/data/deepemhancer_maps_test'
    emready_dir = '/home/abharadwaj1/papers/elife_paper/figure_information/outputs/emready_outputs'

    # Output directory
    output_dir = '/home/abharadwaj1/papers/elife_paper/figure_information/outputs/locscale_with_deepemhancer_emready'
    num_processors = 6

    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Collect all unique emdb_pdb pairs from the reference directories
    reference_dirs = [deepemhancer_dir, emready_dir]
    emdb_pdb_pairs = get_emdb_pdb_pairs(reference_dirs)

    references = ['deepemhancer', 'emready']

    # Before running, check if all required maps exist
    missing_files = []

    for emdb_id_padded, pdb_id in emdb_pdb_pairs:
        # Convert EMDB ID to integer and back to string to remove leading zeros
        emdb_id_int = str(int(emdb_id_padded))
        em_map_filename = f'EMD_{emdb_id_int}_unsharpened_fullmap.mrc'
        em_map_path = os.path.join(unsharpened_maps_dir, em_map_filename)

        # Mask filename (EMDB ID padded with leading zeros)
        mask_filename = f'emd_{emdb_id_padded}_FDR_confidence_final.map'
        mask_path = os.path.join(masks_dir, mask_filename)

        # Check if EM map and mask files exist
        if not os.path.exists(em_map_path):
            missing_files.append(em_map_path)
        if not os.path.exists(mask_path):
            missing_files.append(mask_path)

        for ref in references:
            if ref == 'deepemhancer':
                ref_dir = deepemhancer_dir
                ref_filename = f'emd_{emdb_id_padded}_DeepEMhancer_wideTarget.map'
            elif ref == 'emready':
                ref_dir = emready_dir
                ref_filename = f'emd_{emdb_id_padded}_emready_output_resampled.mrc'
            else:
                continue

            subfolder = f'{emdb_id_padded}_{pdb_id}'
            reference_map_path = os.path.join(ref_dir, subfolder, ref_filename)

            if not os.path.exists(reference_map_path):
                missing_files.append(reference_map_path)

    # If there are missing files, raise an assertion error
    if missing_files:
        missing_files_str = '\n'.join(missing_files)
        print(f"The following required files are missing:\n{missing_files_str}")

    # If all files exist, proceed to run locscale
    for emdb_id_padded, pdb_id in emdb_pdb_pairs:
        emdb_id_int = str(int(emdb_id_padded))
        em_map_filename = f'EMD_{emdb_id_int}_unsharpened_fullmap.mrc'
        em_map_path = os.path.join(unsharpened_maps_dir, em_map_filename)

        mask_filename = f'emd_{emdb_id_padded}_FDR_confidence_final.map'
        mask_path = os.path.join(masks_dir, mask_filename)

        for ref in references:
            if ref == 'deepemhancer':
                ref_dir = deepemhancer_dir
                ref_filename = f'emd_{emdb_id_padded}_DeepEMhancer_wideTarget.map'
            elif ref == 'emready':
                ref_dir = emready_dir
                ref_filename = f'emd_{emdb_id_padded}_emready_output_resampled.mrc'
            else:
                continue

            subfolder = f'{emdb_id_padded}_{pdb_id}'
            reference_map_path = os.path.join(ref_dir, subfolder, ref_filename)

            # Construct the output filename and path
            output_filename = f'locscale_with_{ref}_emdb_{emdb_id_padded}_{pdb_id}.mrc'
            output_path = os.path.join(output_dir, output_filename)

            # Construct the output processing folder path
            processing_folder_name = f'processing_files_{emdb_id_padded}_{ref}'
            processing_folder_path = os.path.join(output_dir, processing_folder_name)

            # Ensure the processing folder exists
            os.makedirs(processing_folder_path, exist_ok=True)

            # Build the locscale command with the new '-op' parameter
            cmd = [
                'locscale',
                '-em', em_map_path,
                '-ma', mask_path,
                '-mm', reference_map_path,
                '-v',
                '-o', output_path,
                '-op', processing_folder_path,
                '-np', str(num_processors)
            ]

            cmd_str = ' '.join(cmd)

            try:
                if args.dry_run:
                    print(f"Dry run command:\n{cmd_str}\n")
                else:
                    print(f"Running locscale for EMDB ID {emdb_id_padded} with reference '{ref}' and PDB ID '{pdb_id}'...")
                    subprocess.run(cmd, check=True)
                    print(f"locscale completed for EMDB ID {emdb_id_padded} with reference '{ref}'.\n")
            except subprocess.CalledProcessError as e:
                print(f"Error running locscale for EMDB ID {emdb_id_padded} with reference '{ref}': {e}\n")
            except Exception as e:
                print(f"Unexpected error: {e}\n")

if __name__ == "__main__":
    main()
