import os
import sys
import random
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from scipy.stats import pearsonr
from locscale.include.emmer.ndimage.map_utils import (
    extract_window,
    load_map,
    get_all_voxels_inside_mask,
    resample_map,
)
from locscale.include.emmer.ndimage.filter import get_cosine_mask
from locscale.include.emmer.ndimage.fsc_util import (
    calculate_phase_correlation_maps,
    calculate_amplitude_correlation_maps,
)
from locscale.include.emmer.ndimage.profile_tools import (
    frequency_array,
    compute_radial_profile,
)
from locscale.emmernet.run_emmernet import load_emmernet_model

random.seed(42)

def preprocess_map(emmap, apix, standardize=True):
    ## Resample the map to 1A per pixel
    emmap_resampled = resample_map(emmap, apix=apix, apix_new=1)
    ## Standardize the map
    if standardize:
        emmap_standardized = (emmap_resampled - np.mean(emmap_resampled)) / np.std(emmap_resampled)
        return emmap_standardized
    else:
        return emmap_resampled

def main():
    parser = argparse.ArgumentParser(description='Process EM maps and compute correlations.')
    parser.add_argument('--unsharp_path', required=True, help='Path to the unsharp map')
    parser.add_argument('--target_path', required=True, help='Path to the target map')
    parser.add_argument('--mask_path', required=True, help='Path to the mask map')

    args = parser.parse_args()

    ## Load the model
    model_path = "/home/abharadwaj1/dev/locscale/locscale/emmernet/emmernet_models/emmernet/EMmerNet_highContext.hdf5"
    emmernet_type = "emmernet_high_context"
    emmernet_model_folder = os.path.dirname(model_path)
    verbose = True
    cuda_visible_devices_string = ""
    input_dictionary = {
        "trained_model": emmernet_type,
        "emmernet_model_folder": emmernet_model_folder,
        "verbose": verbose,
        "model_path": model_path,
        "cuda_visible_devices_string": cuda_visible_devices_string
    }

    emmernet_model = load_emmernet_model(input_dictionary)
    print("Loaded the model from {}".format(model_path))

    emmap_path_unsharp = os.path.abspath(args.unsharp_path)
    emmap_path_target = os.path.abspath(args.target_path)
    mask_path = os.path.abspath(args.mask_path)

    # Load maps
    emmap_unsharp_raw, apix_unsharp = load_map(emmap_path_unsharp)
    emmap_target_raw, apix_target = load_map(emmap_path_target)
    mask_raw, apix_mask = load_map(mask_path)

    # Preprocess maps
    emmap_unsharp = preprocess_map(emmap_unsharp_raw, apix_unsharp, standardize=True)
    emmap_target = preprocess_map(emmap_target_raw, apix_target, standardize=True)
    mask = preprocess_map(mask_raw, apix_mask, standardize=False)

    # Binarize and smooth mask
    mask_binarized = (mask > 0.99).astype(np.int_)
    mask_smooth = get_cosine_mask(mask_binarized, 5)

    # Get all voxels inside mask
    all_voxels = get_all_voxels_inside_mask(mask, mask_threshold=0.99)
    print('Number of voxels inside mask: {}'.format(len(all_voxels)))

    # Sample 100 random voxels
    num_samples = 100
    if len(all_voxels) < num_samples:
        print("Warning: Number of voxels inside mask is less than 100.")
        num_samples = len(all_voxels)
    sampled_voxels = random.sample(all_voxels, num_samples)

    print("Sampled voxels (first 3):", sampled_voxels[:3])

    window_size_pix = 32  # Since we resampled to apix=1, window size is 32 pixels

    # Initialize lists to store correlations
    phase_correlations_unsharp = []
    amplitude_correlations_unsharp = []
    phase_correlations_target = []
    amplitude_correlations_target = []
    phase_correlations_unsharp_target = []
    amplitude_correlations_unsharp_target = []

    # Lists to store Pearson correlations of radial profiles
    pearson_correlations_target_pred = []
    pearson_correlations_unsharp_pred = []

    # Lists to store radial profile differences
    radial_profile_differences_target = []
    radial_profile_differences_unsharp = []

    batch_size = 10  # Set batch size to 10

    # Iterate over sampled voxels in batches
    for i in range(0, len(sampled_voxels), batch_size):
        batch_voxels = sampled_voxels[i:i+batch_size]
        num_cubes = len(batch_voxels)
        cube_size = window_size_pix

        # Initialize arrays to store the windows
        cubes_batch_X = np.empty((num_cubes, cube_size, cube_size, cube_size, 1))
        windows_unsharp = []
        windows_target = []

        for idx, center in enumerate(batch_voxels):
            print(f"Processing voxel {i+idx+1}/{num_samples} at position {center}")

            # Extract windows
            window_unsharp = extract_window(emmap_unsharp, center, window_size_pix)
            window_target = extract_window(emmap_target, center, window_size_pix)

            windows_unsharp.append(window_unsharp)
            windows_target.append(window_target)

            # Add window to batch array
            cubes_batch_X[idx] = window_unsharp[..., np.newaxis]

        # Predict using the model on the batch
        cubes_batch_predicted = emmernet_model.predict(
            x=cubes_batch_X, batch_size=batch_size, verbose=0
        )

        # Process predictions
        cubes_batch_predicted = np.squeeze(cubes_batch_predicted, axis=-1)  # Shape: (num_cubes, cube_size, cube_size, cube_size)

        for idx in range(num_cubes):
            prediction_cube = cubes_batch_predicted[idx]
            window_unsharp = windows_unsharp[idx]
            window_target = windows_target[idx]

            # Compute correlations with unsharp map window
            phase_corr_unsharp = calculate_phase_correlation_maps(window_unsharp, prediction_cube)
            amplitude_corr_unsharp = calculate_amplitude_correlation_maps(window_unsharp, prediction_cube)

            phase_correlations_unsharp.append(phase_corr_unsharp)
            amplitude_correlations_unsharp.append(amplitude_corr_unsharp)

            # Compute correlations with target map window
            phase_corr_target = calculate_phase_correlation_maps(window_target, prediction_cube)
            amplitude_corr_target = calculate_amplitude_correlation_maps(window_target, prediction_cube)

            phase_correlations_target.append(phase_corr_target)
            amplitude_correlations_target.append(amplitude_corr_target)

            # Compute correlations between unsharp and target windows
            phase_corr_unsharp_target = calculate_phase_correlation_maps(window_unsharp, window_target)
            amplitude_corr_unsharp_target = calculate_amplitude_correlation_maps(window_unsharp, window_target)

            phase_correlations_unsharp_target.append(phase_corr_unsharp_target)
            amplitude_correlations_unsharp_target.append(amplitude_corr_unsharp_target)

            # Compute radial profiles
            rp_pred = compute_radial_profile(prediction_cube)
            rp_target = compute_radial_profile(window_target)
            rp_unsharp = compute_radial_profile(window_unsharp)

            # Compute Pearson correlations of radial profiles
            corr_target_pred, _ = pearsonr(np.log(rp_target), np.log(rp_pred))
            corr_unsharp_pred, _ = pearsonr(np.log(rp_unsharp), np.log(rp_pred))

            pearson_correlations_target_pred.append(corr_target_pred)
            pearson_correlations_unsharp_pred.append(corr_unsharp_pred)

            # Compute difference in radial profiles between target and prediction
            diff_rp_target_pred = rp_target - rp_pred
            radial_profile_differences_target.append(diff_rp_target_pred)

            # Compute difference in radial profiles between unsharp and prediction
            diff_rp_unsharp_pred = rp_unsharp - rp_pred
            radial_profile_differences_unsharp.append(diff_rp_unsharp_pred)

    # Convert lists to numpy arrays
    phase_correlations_unsharp = np.array(phase_correlations_unsharp)
    amplitude_correlations_unsharp = np.array(amplitude_correlations_unsharp)
    phase_correlations_target = np.array(phase_correlations_target)
    amplitude_correlations_target = np.array(amplitude_correlations_target)
    phase_correlations_unsharp_target = np.array(phase_correlations_unsharp_target)
    amplitude_correlations_unsharp_target = np.array(amplitude_correlations_unsharp_target)
    radial_profile_differences_target = np.array(radial_profile_differences_target)
    radial_profile_differences_unsharp = np.array(radial_profile_differences_unsharp)

    # Frequency array using frequency_array function
    apix = 1  # Since maps have been resampled to apix=1
    freq = frequency_array(phase_correlations_unsharp[0], apix)

    # Function to plot correlations
    def plot_correlations(freq, phase_corrs, amp_corrs, title_suffix, save_suffix):
        mean_phase_corr = np.mean(phase_corrs, axis=0)
        std_phase_corr = np.std(phase_corrs, axis=0)
        mean_amp_corr = np.mean(amp_corrs, axis=0)
        std_amp_corr = np.std(amp_corrs, axis=0)

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))

        # Phase Correlation Plot
        ax1.plot(freq, mean_phase_corr, color='blue', label='Mean Phase Correlation')
        ax1.fill_between(freq, mean_phase_corr - std_phase_corr, mean_phase_corr + std_phase_corr, color='blue', alpha=0.3)
        ax1.set_title(f'Phase Correlation {title_suffix}')
        ax1.set_xlabel(r'Spatial frequency ($\AA^{-1}$)')
        ax1.set_ylabel('Phase Correlation')
        ax1.legend()
        ax1.grid(True)
        ax1.set_ylim(-0.2, 1.2)  # Set y-axis limits

        # Amplitude Correlation Plot
        ax2.plot(freq, mean_amp_corr, color='red', label='Mean Amplitude Correlation')
        ax2.fill_between(freq, mean_amp_corr - std_amp_corr, mean_amp_corr + std_amp_corr, color='red', alpha=0.3)
        ax2.set_title(f'Amplitude Correlation {title_suffix}')
        ax2.set_xlabel(r'Spatial frequency ($\AA^{-1}$)')
        ax2.set_ylabel('Amplitude Correlation')
        ax2.legend()
        ax2.grid(True)
        ax2.set_ylim(-0.2, 1.2)  # Set y-axis limits

        plt.tight_layout()
        save_path = os.path.join(
            os.path.dirname(emmap_path_unsharp),
            f'correlations_{save_suffix}.png'
        )
        plt.savefig(save_path, dpi=300)
        print(f'Saved correlation plot to {save_path}')
        plt.close()

    # Plot correlations with respect to unsharp map
    plot_correlations(
        freq,
        phase_correlations_unsharp,
        amplitude_correlations_unsharp,
        title_suffix='with Unsharp Map',
        save_suffix='unsharp'
    )

    # Plot correlations with respect to target map
    plot_correlations(
        freq,
        phase_correlations_target,
        amplitude_correlations_target,
        title_suffix='with Target Map',
        save_suffix='target'
    )

    # Plot correlations between unsharp and target windows
    plot_correlations(
        freq,
        phase_correlations_unsharp_target,
        amplitude_correlations_unsharp_target,
        title_suffix='between Unsharp and Target Maps',
        save_suffix='unsharp_target'
    )

    # Create box plot with swarm plot for Pearson correlations
    # Prepare data for plotting
    data = {
        'Correlation': pearson_correlations_target_pred + pearson_correlations_unsharp_pred,
        'Pair': ['Target-Predicted'] * len(pearson_correlations_target_pred) + ['Unsharp-Predicted'] * len(pearson_correlations_unsharp_pred)
    }
    df = pd.DataFrame(data)

    # Plotting
    plt.figure(figsize=(8, 6))
    sns.boxplot(x='Pair', y='Correlation', data=df)
    sns.swarmplot(x='Pair', y='Correlation', data=df, color='.25')
    plt.title('Pearson Correlation of Radial Profiles')
    plt.ylabel('Pearson Correlation Coefficient')
    plt.grid(True)
    plt.ylim(-0.2, 1.2)  # Set y-axis limits

    # Save the figure
    save_path = os.path.join(
        os.path.dirname(emmap_path_unsharp),
        'pearson_correlations_radial_profiles.png'
    )
    plt.savefig(save_path, dpi=300)
    print(f'Saved Pearson correlation plot to {save_path}')
    plt.close()

    # Compute mean and std of radial profile differences
    mean_diff_rp_target = np.mean(radial_profile_differences_target, axis=0)
    std_diff_rp_target = np.std(radial_profile_differences_target, axis=0)

    mean_diff_rp_unsharp = np.mean(radial_profile_differences_unsharp, axis=0)
    std_diff_rp_unsharp = np.std(radial_profile_differences_unsharp, axis=0)

    # Since radial profiles may have different lengths, ensure frequency axis matches
    freq_rp = np.linspace(0, 0.5, len(mean_diff_rp_target))

    # Plot mean differences with shaded std for both target and unsharp
    plt.figure(figsize=(8, 6))

    plt.plot(freq_rp, mean_diff_rp_target, color='green', label='Target - Prediction')
    plt.fill_between(freq_rp, mean_diff_rp_target - std_diff_rp_target, mean_diff_rp_target + std_diff_rp_target, color='green', alpha=0.3)

    plt.plot(freq_rp, mean_diff_rp_unsharp, color='blue', label='Unsharp - Prediction')
    plt.fill_between(freq_rp, mean_diff_rp_unsharp - std_diff_rp_unsharp, mean_diff_rp_unsharp + std_diff_rp_unsharp, color='blue', alpha=0.3)

    plt.title('Mean Difference in Radial Profiles')
    plt.xlabel(r'Spatial frequency ($\AA^{-1}$)')
    plt.ylabel('Difference in Amplitude')
    plt.legend()
    plt.grid(True)

    # Set y-axis to symlog scale to handle positive and negative values
    plt.yscale('symlog', linthresh=1e-3)
   # plt.ylim(-np.max(np.abs([mean_diff_rp_target, mean_diff_rp_unsharp])) * 1.1, np.max(np.abs([mean_diff_rp_target, mean_diff_rp_unsharp])) * 1.1)

    # Save the figure
    save_path = os.path.join(
        os.path.dirname(emmap_path_unsharp),
        'mean_difference_radial_profiles.png'
    )
    plt.savefig(save_path, dpi=300)
    print(f'Saved mean difference plot to {save_path}')
    plt.close()

if __name__ == '__main__':
    main()
