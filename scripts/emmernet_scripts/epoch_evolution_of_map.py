# Script to study the evolution of the map with epochs
import os 
import subprocess
import argparse 
import numpy as np

import shutil

def get_emmernet_command(emmap_path, model_path, output_map_path, gpu_ids):
    cmd = []
    locscale_path = os.environ["LOCSCALE_PATH"]
    cmd.append("python")
    cmd.append(locscale_path)
    #cmd.append("feature_enhance")
    cmd.append("--emmap_path")
    cmd.append(emmap_path)
    cmd.append("--model_path")
    cmd.append(model_path)
    cmd.append("--outfile")
    cmd.append(output_map_path)
    cmd.append("--verbose")
    cmd.append("-gpus")
    
    if len(gpu_ids) == 1:
        cmd.append(gpu_ids[0])
    else:
        for gpu_id in gpu_ids:
            cmd.append(gpu_id)
    cmd.append("-np")
    cmd.append("6")
    return cmd



parser = argparse.ArgumentParser(description='Script to study the evolution of the map with epochs')
parser.add_argument('--emmap_path', type=str, help='Path to the input map', required=True)
parser.add_argument('--output_dir', type=str, help='Path to the output directory', default=None)
parser.add_argument('--epochs', type=int, nargs = '+', help='Epochs to study', default=None)
parser.add_argument('--epoch_model_folder', type=str, help='Path to the folder containing the epoch models', default=None)
parser.add_argument('--epoch_model_format', type=str, help='Format of the epoch models (use * to indicate epoch number)', default="hybrid_version_C_epoch-*.hdf5")

parser.add_argument('--gpus', type=str, nargs='+', help='GPUs to use', required=True)
                    
args = parser.parse_args()

emmap_path = args.emmap_path
output_dir = args.output_dir

emmap_path_absolute = os.path.abspath(emmap_path)

if output_dir is None:
    output_dir_absolute = os.path.join(os.path.dirname(emmap_path_absolute), "epoch_evolution")
else:
    output_dir_absolute = os.path.abspath(output_dir)
    if not os.path.exists(output_dir_absolute):
        os.mkdir(output_dir_absolute)

assert os.path.exists(emmap_path_absolute), "Input map does not exist"
assert os.path.exists(output_dir_absolute), "Output directory does not exist"

epochs = args.epochs

if epochs is None:
    epochs = list(np.arange(0, 20, 1))

gpu_ids = args.gpus

if args.epoch_model_folder is None:
    epoch_model_folder = "/home/abharadwaj1/dev/map_sharpening/emmernet/emmernet_training/hybrid_version_C/outputdata/hybrid_version_C/saved_models"
else:
    epoch_model_folder = args.epoch_model_folder

epoch_model_format = args.epoch_model_format

assert os.path.exists(epoch_model_folder), "Epoch model folder does not exist"

epoch_model_format = epoch_model_format.replace("*", "{}")

for epoch in epochs: 
    epoch_model_path = os.path.join(epoch_model_folder, epoch_model_format.format(epoch))
    assert os.path.exists(epoch_model_path), "Epoch model path does not exist: {}".format(epoch_model_path)

for epoch in epochs: 
    epoch_model_path = os.path.join(epoch_model_folder, epoch_model_format.format(epoch))

    output_dir_epoch = os.path.join(output_dir_absolute, "epoch_{}".format(epoch))
    if not os.path.exists(output_dir_epoch):
        os.makedirs(output_dir_epoch)
    

    output_map_path = os.path.join(output_dir_epoch, "output_map_epoch_{}.mrc".format(epoch))
    emmap_path_copy = shutil.copy(emmap_path_absolute, output_dir_epoch)
    cmd = get_emmernet_command(emmap_path_copy, epoch_model_path, output_map_path, gpu_ids)

    print("Running command: {}".format(" ".join(cmd)))
    try:
        p = subprocess.run(cmd, shell=False, check=True)
    except subprocess.CalledProcessError as e:
        print("Error running command: {}".format(" ".join(cmd)))
        raise e
    
    print("Completed epoch: {}".format(epoch))

print("===========================================")
print("Completed all epochs")
print("===========================================")