# Script to run refmac refinement on a model and calculate the fsc values for 50 iterations
from locscale.include.emmer.ndimage.fsc_util import calculate_fsc_maps
from locscale.preprocessing.headers import run_servalcat_iterative
from locscale.include.emmer.pdb.pdb_to_map import pdb2map
from locscale.include.emmer.ndimage.map_utils import save_as_mrc, load_map
import os
import numpy as np
import sys 

refmac_iterations = np.arange(1, 50, dtype=int)
def copy_files_to_folder(file, folder):
    import shutil
    copied_file_path = shutil.copy(file, folder)
    return copied_file_path
    

model_path = sys.argv[1]
halfmap1_path = sys.argv[2]
halfmap2_path = sys.argv[3]
resolution = float(sys.argv[4])

num_iter = 50
emmap,apix = load_map(halfmap1_path)

input_folder = os.path.dirname(model_path)
input_folder = os.path.abspath(input_folder)
output_folder = os.path.join(input_folder, 'refmac_iteration_{}'.format(num_iter))

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

copied_map_path = copy_files_to_folder(halfmap1_path, output_folder)
copied_map2_path = copy_files_to_folder(halfmap2_path, output_folder)
copied_model_path = copy_files_to_folder(model_path, output_folder)

refined_model_path = run_servalcat_iterative(
    model_path=copied_model_path, map_path=copied_map_path, pseudomodel_refinement=True,
                resolution=resolution, num_iter=num_iter)

refined_model_per_iteration = {k : os.path.join(output_folder,f"servalcat_refinement_cycle_{k}_servalcat_refined.pdb") for k in refmac_iterations}

fsc_average_halfmap2 = {}
fsc_average_halfmap1 = {}
for i, refined in refined_model_per_iteration.items():
    refined_model_map = pdb2map(refined, apix=apix, size=emmap.shape)
    save_as_mrc(refined_model_map, os.path.join(output_folder, f"refined_model_map_{i}.mrc"), apix=apix)

    fsc_vals_halfmap1 = calculate_fsc_maps(refined_model_map, halfmap1_path)
    fsc_vals_halfmap2 = calculate_fsc_maps(refined_model_map, halfmap2_path)

    fsc_average_halfmap1[i] = (np.mean(fsc_vals_halfmap1), fsc_vals_halfmap1)
    fsc_average_halfmap2[i] = (np.mean(fsc_vals_halfmap2), fsc_vals_halfmap2)

    print(f"{i} : {np.mean(fsc_vals_halfmap1)} : {np.mean(fsc_vals_halfmap2)}")

# Dump the fsc values to a file in the input folder
import pickle
with open(os.path.join(input_folder, 'fsc_average_halfmap_2_cycle50.pickle'), 'wb') as f:
    pickle.dump(fsc_average_halfmap2, f)
with open(os.path.join(input_folder, 'fsc_average_halfmap_1_cycle50.pickle'), 'wb') as f:
    pickle.dump(fsc_average_halfmap1, f)



    
