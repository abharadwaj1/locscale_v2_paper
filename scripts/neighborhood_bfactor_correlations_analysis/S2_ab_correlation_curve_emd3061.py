# Script to generate correlation curve for the EMD3061 data 

import matplotlib.pyplot as plt
from locscale.include.emmer.pdb.pdb_tools import neighborhood_bfactor_correlation_sample, neighborhood_bfactor_correlation
from locscale.include.emmer.ndimage.map_tools import get_atomic_model_mask
from tqdm import tqdm
import seaborn as sns
import gemmi
import os

def plot_correlations(x_array, y_array,  x_label, y_label, title_text, \
                    scatter=True, figsize_cm=(14,8),font="Helvetica",fontsize=10,\
                    fontscale=1,hue=None,find_correlation=True, alpha=0.3, filepath=None):

    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    from locscale.include.emmer.ndimage.profile_tools import crop_profile_between_frequency
    import seaborn as sns
    from scipy import stats
    import matplotlib 
    import pandas as pd
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    # set the global font size for the plot

        
    plt.rcParams.update({'font.size': fontsize})
    figsize = (figsize_cm[0]/2.54, figsize_cm[1]/2.54) # convert cm to inches
    
    fig, ax = plt.subplots(figsize=figsize, dpi=600) # dpi=600 for publication quality
    sns.set_theme(context="paper", font=font, font_scale=fontscale)
    # Set font size for all text in the figure
    sns.set_style("white")
    
    def annotate(data, **kws):
        pearson_correlation = stats.pearsonr(x_array, y_array)
        r2_text = f"$R$ = {pearson_correlation[0]:.2f}"
        ax = plt.gca()
        ax.text(.05, .8, r2_text,transform=ax.transAxes)
    # Create a pandas dataframe for the data
    data = pd.DataFrame({x_label: x_array, y_label: y_array})
    # Plot the data    
    g = sns.lmplot(x=x_label, y=y_label, data=data, scatter=scatter, ci=95, markers=".")
    g.map_dataframe(annotate)

        

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.tight_layout()

    
    plt.savefig(filepath, bbox_inches='tight')

def pretty_lineplot_XY(xdata, ydata, xlabel, ylabel, figsize_cm=(14,8),fontsize=10, \
                        marker="o", markersize=3,fontscale=1,font="Helvetica", \
                        linewidth=1,legends=None):
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    from locscale.include.emmer.ndimage.profile_tools import crop_profile_between_frequency
    import seaborn as sns
    import matplotlib 
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    # set the global font size for the plot

        
    plt.rcParams.update({'font.size': fontsize})
    figsize = (figsize_cm[0]/2.54, figsize_cm[1]/2.54) # convert cm to inches
    
    fig, ax = plt.subplots(figsize=figsize, dpi=600)  # DPI is fixed to 600 for publication quality
    sns.set_theme(context="paper", font=font, font_scale=fontscale)
    # Set font size for all text in the figure
    sns.set_style("white")

    sns.lineplot(x=xdata,y=ydata,linewidth=linewidth,marker=marker,markersize=markersize, ax=ax)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel, rotation=90, ha="center")

    if legends is not None:        
        ax.legend(legends)
    plt.tight_layout()
    plt.ylim(0.2,1.2)
    plt.yticks([0.5,1.0])

    return fig


def pretty_lineplot_XY_multiple(xdata_list, ydata_list, xlabel, ylabel, figsize_cm=(14,8),fontsize=10, \
                        marker="o", markersize=3,fontscale=1,font="Helvetica", \
                        linewidth=1,legends=None, title=None):
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    from locscale.include.emmer.ndimage.profile_tools import crop_profile_between_frequency
    import seaborn as sns
    import matplotlib 
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    # set the global font size for the plot

        
    plt.rcParams.update({'font.size': fontsize})
    figsize = (figsize_cm[0]/2.54, figsize_cm[1]/2.54) # convert cm to inches
    
    fig, ax = plt.subplots(figsize=figsize, dpi=600)  # DPI is fixed to 600 for publication quality
    sns.set_theme(context="paper", font=font, font_scale=fontscale)
    # Set font size for all text in the figure
    sns.set_style("white")
    i = 0
    for xdata, ydata in zip(xdata_list, ydata_list):
        sns.lineplot(x=xdata,y=ydata,linewidth=linewidth, ax=ax)
        i += 1
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel, rotation=90, ha="center")
    # Show the legend
    # ax.legend()
    plt.tight_layout()
    plt.ylim(0.2,1.2)
    plt.yticks([0.5,1.0])

    if title is not None:
        plt.title(title)
    return fig

output_folder = "/home/abharadwaj1/papers/elife_paper/figure_information/scripts/supplementary_2/combined_nolabel"
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
emmap_path = "/home/abharadwaj1/papers/elife_paper/figure_information/inputs/emd_3061/EMD_3061_unfiltered.mrc"
atomic_model_path = "/home/abharadwaj1/papers/elife_paper/figure_information/scripts/supplementary_2/5a63_shifted_servalcat_refined.pdb"
iterative_refined_pseudo_model_path = "/home/abharadwaj1/papers/elife_paper/figure_information/scripts/supplementary_2/EMD_3061_unfiltered_confidenceMap_gradient_pseudomodel_proper_element_composition.pdb"
non_averaged_pseudo_model_path = "/home/abharadwaj1/papers/elife_paper/figure_information/scripts/supplementary_2/fdr_soft_gradient_pseudomodel_servalcat_refined.pdb"

atomic_model_map_path_in = atomic_model_path.replace(".pdb", "_atomic_mask.mrc")
atomic_model_map_path = get_atomic_model_mask(emmap_path=emmap_path, pdb_path=atomic_model_path, output_filename=atomic_model_map_path_in)

bfactor_neighborhood_correlation_atomic = neighborhood_bfactor_correlation(atomic_model_path,max_radius=10, num_steps=10)

bfactor_neighborhood_correlation_iterative_refined_pseudo = neighborhood_bfactor_correlation(iterative_refined_pseudo_model_path,max_radius=10, num_steps=10, mask_path=None, invert=False)
bfactor_neighborhood_correlation_iterative_refined_pseudo_ordered = neighborhood_bfactor_correlation(iterative_refined_pseudo_model_path,max_radius=10, num_steps=10, mask_path=atomic_model_map_path, invert=False)
bfactor_neighborhood_correlation_iterative_refined_pseudo_disordered = neighborhood_bfactor_correlation(iterative_refined_pseudo_model_path,max_radius=10, num_steps=10, mask_path=atomic_model_map_path, invert=True)

bfactor_neighborhood_correlation_non_averaged_pseudo = neighborhood_bfactor_correlation(non_averaged_pseudo_model_path,max_radius=10, num_steps=10, mask_path=None, invert=False)
bfactor_neighborhood_correlation_non_averaged_pseudo_ordered = neighborhood_bfactor_correlation(non_averaged_pseudo_model_path,max_radius=10, num_steps=10, mask_path=atomic_model_map_path, invert=False)
bfactor_neighborhood_correlation_non_averaged_pseudo_disordered = neighborhood_bfactor_correlation(non_averaged_pseudo_model_path,max_radius=10, num_steps=10, mask_path=atomic_model_map_path, invert=True)


# Plot the correlation curve individually
fig_format = "eps"
fig_atomic_path = os.path.join(output_folder, "S2_ab_correlation_curve_atomic.{}".format(fig_format))
fig_iterative_refined_pseudo_path = os.path.join(output_folder, "S2_ab_correlation_curve_iterative_refined_pseudo.{}".format(fig_format))

fig_non_averaged_pseudo_path = os.path.join(output_folder, "S2_ab_correlation_curve_non_averaged_pseudo.{}".format(fig_format))
fig_atomic_non_averaged_path = os.path.join(output_folder, "S2_ab_correlation_curve_atomic_non_averaged.{}".format(fig_format))
fig_atomic_iterative_ordered_disordered_path = os.path.join(output_folder, "S2_ab_correlation_curve_atomic_iterative_ordered_disordered.{}".format(fig_format))
fig_atomic_non_averaged_ordered_disordered_path = os.path.join(output_folder, "S2_ab_correlation_curve_atomic_non_averaged_ordered_disordered.{}".format(fig_format))
fig_atomic_iterative_non_averaged_path = os.path.join(output_folder, "S2_ab_correlation_curve_atomic_iterative_non_averaged.{}".format(fig_format))
fig_atomic_correlations_radius_low_path = os.path.join(output_folder, "S2_cd_correlation_atomic_radius_low.{}".format(fig_format))
fig_iterative_refined_pseudo_correlations_radius_low_path = os.path.join(output_folder, "S2_cd_correlation_iterative_refined_pseudo_radius_low.{}".format(fig_format))
fig_non_averaged_pseudo_correlations_radius_low_path = os.path.join(output_folder, "S2_cd_correlation_non_averaged_pseudo_radius_low.{}".format(fig_format))
fig_atomic_correlations_radius_high_path = os.path.join(output_folder, "S2_cd_correlation_atomic_radius_high.{}".format(fig_format))
fig_iterative_refined_pseudo_correlations_radius_high_path = os.path.join(output_folder, "S2_cd_correlation_iterative_refined_pseudo_radius_high.{}".format(fig_format))
fig_non_averaged_pseudo_correlations_radius_high_path = os.path.join(output_folder, "S2_cd_correlation_non_averaged_pseudo_radius_high.{}".format(fig_format))

fig_non_averaged_ordered_disordered_path = os.path.join(output_folder, "S2_cd_correlation_non_averaged_ordered_disordered.{}".format(fig_format))
fig_iterative_ordered_disordered_path = os.path.join(output_folder, "S2_cd_correlation_iterative_ordered_disordered.{}".format(fig_format))


figsize_cm = (8,8)
fsize = 8

radii = bfactor_neighborhood_correlation_atomic.keys()
correlations_atomic = [x[2][0] for x in bfactor_neighborhood_correlation_atomic.values()]
correlations_iterative_refined_pseudo = [x[2][0] for x in bfactor_neighborhood_correlation_iterative_refined_pseudo.values()]
correlations_iterative_refined_pseudo_ordered = [x[2][0] for x in bfactor_neighborhood_correlation_iterative_refined_pseudo_ordered.values()]
correlations_iterative_refined_pseudo_disordered = [x[2][0] for x in bfactor_neighborhood_correlation_iterative_refined_pseudo_disordered.values()]

correlations_non_averaged_pseudo = [x[2][0] for x in bfactor_neighborhood_correlation_non_averaged_pseudo.values()]
correlations_non_averaged_pseudo_ordered = [x[2][0] for x in bfactor_neighborhood_correlation_non_averaged_pseudo_ordered.values()]
correlations_non_averaged_pseudo_disordered = [x[2][0] for x in bfactor_neighborhood_correlation_non_averaged_pseudo_disordered.values()]

fig_atomic = pretty_lineplot_XY(radii, correlations_atomic, xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                                figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1)

fig_atomic.savefig(fig_atomic_path, bbox_inches="tight")

fig_iterative_refined_pseudo = pretty_lineplot_XY(radii, correlations_iterative_refined_pseudo, xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                                figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1)

fig_iterative_refined_pseudo.savefig(fig_iterative_refined_pseudo_path, bbox_inches="tight")

fig_non_averaged_pseudo = pretty_lineplot_XY(radii, correlations_non_averaged_pseudo, xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                                figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1)

fig_non_averaged_pseudo.savefig(fig_non_averaged_pseudo_path, bbox_inches="tight")

# Plot atomic and non-averaged pseudo correlations lineplot multiple

fig_atomic_non_averaged = pretty_lineplot_XY_multiple([radii, radii], [correlations_atomic, correlations_non_averaged_pseudo], 
                            xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                            figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1, \
                            legends=["Atomic model", "Pseudo-atomic model"])

fig_atomic_non_averaged.savefig(fig_atomic_non_averaged_path, bbox_inches="tight")

fig_atomic_iterative_non_averaged = pretty_lineplot_XY_multiple([radii, radii, radii], 
                            [correlations_atomic, correlations_non_averaged_pseudo, correlations_iterative_refined_pseudo],\
                            xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                            figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1, \
                            legends=["Atomic model", "Non-averaged Pseudo-atomic model","Iterative Pseudo-atomic model"],\
                            title = "EMDB: 3061")

fig_atomic_iterative_non_averaged.savefig(fig_atomic_iterative_non_averaged_path, bbox_inches="tight")

# Plot atomic , non-averaged pseudo ordered and disordered correlations lineplot multiple
fig_atomic_non_averaged_ordered_disordered = pretty_lineplot_XY_multiple([radii, radii, radii],
                            [correlations_atomic, correlations_non_averaged_pseudo_ordered, correlations_non_averaged_pseudo_disordered],\
                            xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                            figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1, \
                            legends=["Atomic model", "Unrestrained refinement (ordered)", "Unrestrained refinement (disordered)"],\
                            title = "EMDB: 3061")
                                                                         
fig_atomic_non_averaged_ordered_disordered.savefig(fig_atomic_non_averaged_ordered_disordered_path, bbox_inches="tight")

# Plot atomic , iterative pseudo ordered and disordered correlations lineplot multiple
fig_atomic_iterative_ordered_disordered = pretty_lineplot_XY_multiple([radii, radii, radii],
                            [correlations_atomic, correlations_iterative_refined_pseudo_ordered, correlations_iterative_refined_pseudo_disordered],\
                            xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                            figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1, \
                            legends=["Atomic model", "Restrained refinement (ordered)", "Restrained refinement (disordered)"],\
                            title = "EMDB: 3061")


fig_atomic_iterative_ordered_disordered.savefig(fig_atomic_iterative_ordered_disordered_path, bbox_inches="tight")

# Plot non averaged pseudo, ordered and disordered correlations lineplot multiple
fig_non_averaged_ordered_disordered = pretty_lineplot_XY_multiple([radii, radii, radii],
                            [correlations_non_averaged_pseudo, correlations_non_averaged_pseudo_ordered, correlations_non_averaged_pseudo_disordered],\
                            xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                            figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1, \
                            legends=["Unrestrained refinement", "Unrestrained refinement (ordered)", "Unrestrained refinement (disordered)"],\
                            title = "EMDB: 3061")

fig_non_averaged_ordered_disordered.savefig(fig_non_averaged_ordered_disordered_path, bbox_inches="tight")

# Plot iterative pseudo, ordered and disordered correlations lineplot multiple
fig_iterative_ordered_disordered = pretty_lineplot_XY_multiple([radii, radii, radii],
                            [correlations_iterative_refined_pseudo, correlations_iterative_refined_pseudo_ordered, correlations_iterative_refined_pseudo_disordered],\
                            xlabel=r"Neighborhood radius ($\AA$)", ylabel="ADP Correlation", \
                            figsize_cm=(8,8),fontsize=fsize, fontscale=1, font="Helvetica", linewidth=1, \
                            legends=["Restrained refinement", "Restrained refinement (ordered)", "Restrained refinement (disordered)"],\
                            title = "EMDB: 3061")

fig_iterative_ordered_disordered.savefig(fig_iterative_ordered_disordered_path, bbox_inches="tight")


low_radius = 2.0
individual_bfactors_atomic_radius_low = bfactor_neighborhood_correlation_atomic[low_radius][0]
neighborhood_bfactors_atomic_radius_low = bfactor_neighborhood_correlation_atomic[low_radius][1]

individual_bfactors_iterative_refined_pseudo_radius_low = bfactor_neighborhood_correlation_iterative_refined_pseudo[low_radius][0]
neighborhood_bfactors_iterative_refined_pseudo_radius_low = bfactor_neighborhood_correlation_iterative_refined_pseudo[low_radius][1]

individual_bfactors_non_averaged_pseudo_radius_low = bfactor_neighborhood_correlation_non_averaged_pseudo[low_radius][0]
neighborhood_bfactors_non_averaged_pseudo_radius_low = bfactor_neighborhood_correlation_non_averaged_pseudo[low_radius][1]

high_radius = 10.0
individual_bfactors_atomic_radius_high = bfactor_neighborhood_correlation_atomic[high_radius][0]
neighborhood_bfactors_atomic_radius_high = bfactor_neighborhood_correlation_atomic[high_radius][1]

individual_bfactors_iterative_refined_pseudo_radius_high = bfactor_neighborhood_correlation_iterative_refined_pseudo[high_radius][0]
neighborhood_bfactors_iterative_refined_pseudo_radius_high = bfactor_neighborhood_correlation_iterative_refined_pseudo[high_radius][1]

individual_bfactors_non_averaged_pseudo_radius_high = bfactor_neighborhood_correlation_non_averaged_pseudo[high_radius][0]
neighborhood_bfactors_non_averaged_pseudo_radius_high = bfactor_neighborhood_correlation_non_averaged_pseudo[high_radius][1]


fig_atomic_correlations_radius_low = plot_correlations(
    individual_bfactors_atomic_radius_low, neighborhood_bfactors_atomic_radius_low, 
    x_label=r"Atomic ADP $\AA^2$", y_label=r"$\langle$ ADP $\rangle$ ($\AA^2$)",
    title_text = r"r={0} $\AA$".format(low_radius),
    figsize_cm=figsize_cm,fontsize=fsize, fontscale=1, font="Helvetica", filepath=fig_atomic_correlations_radius_low_path)


fig_iterative_refined_pseudo_correlations_radius_low = plot_correlations(
    individual_bfactors_iterative_refined_pseudo_radius_low, neighborhood_bfactors_iterative_refined_pseudo_radius_low,
    x_label=r"Atomic ADP $\AA^2$", y_label=r"$\langle$ ADP $\rangle$ ($\AA^2$)",
    title_text = r"r={0} $\AA$".format(low_radius),
    figsize_cm=figsize_cm,fontsize=fsize, fontscale=1, font="Helvetica", filepath=fig_iterative_refined_pseudo_correlations_radius_low_path)


fig_non_averaged_pseudo_correlations_radius_low = plot_correlations(
    individual_bfactors_non_averaged_pseudo_radius_low, neighborhood_bfactors_non_averaged_pseudo_radius_low,
    x_label=r"Atomic ADP $\AA^2$", y_label=r"$\langle$ ADP $\rangle$ ($\AA^2$)",
    title_text = r"r={0} $\AA$".format(low_radius),
    figsize_cm=figsize_cm,fontsize=fsize, fontscale=1, font="Helvetica", filepath=fig_non_averaged_pseudo_correlations_radius_low_path)

fig_atomic_correlations_radius_high = plot_correlations(
    individual_bfactors_atomic_radius_high, neighborhood_bfactors_atomic_radius_high,
    x_label=r"Atomic ADP $\AA^2$", y_label=r"$\langle$ ADP $\rangle$ ($\AA^2$)",
    title_text = r"r={0} $\AA$".format(high_radius),
    figsize_cm=figsize_cm,fontsize=fsize, fontscale=1, font="Helvetica", filepath=fig_atomic_correlations_radius_high_path)

fig_iterative_refined_pseudo_correlations_radius_high = plot_correlations(
    individual_bfactors_iterative_refined_pseudo_radius_high, neighborhood_bfactors_iterative_refined_pseudo_radius_high,
    x_label=r"Atomic ADP $\AA^2$", y_label=r"$\langle$ ADP $\rangle$ ($\AA^2$)",
    title_text = r"r={0} $\AA$".format(high_radius),
    figsize_cm=figsize_cm,fontsize=fsize, fontscale=1, font="Helvetica", filepath=fig_iterative_refined_pseudo_correlations_radius_high_path)

fig_non_averaged_pseudo_correlations_radius_high = plot_correlations(
    individual_bfactors_non_averaged_pseudo_radius_high, neighborhood_bfactors_non_averaged_pseudo_radius_high,
    x_label=r"Atomic ADP $\AA^2$", y_label=r"$\langle$ ADP $\rangle$ ($\AA^2$)",
    title_text = r"r={0} $\AA$".format(high_radius),
    figsize_cm=figsize_cm,fontsize=fsize, fontscale=1, font="Helvetica", filepath=fig_non_averaged_pseudo_correlations_radius_high_path)


