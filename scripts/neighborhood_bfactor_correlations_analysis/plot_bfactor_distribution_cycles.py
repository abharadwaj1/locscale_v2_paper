# Script to generate B-factor distribution and IGD fit values for each cycle for EMD 3061 refinement
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import invgamma
from locscale.include.emmer.pdb.pdb_utils import get_bfactors

refined_pdb_paths_with_cycles_for_emdb = {}

emd_3061_processing_files_folder = "/home/abharadwaj1/papers/elife_paper/figure_information/inputs/emd_3061/model_free_proper/model_free_with_nomask/processing_files"

refined_pdb_path_cycles = {}
for cycle in range(1, 10):
    refined_pdb_cycle_path = os.path.join(emd_3061_processing_files_folder, "servalcat_refinement_cycle_{}_servalcat_refined.pdb".format(cycle))
    refined_pdb_path_cycles[cycle] = refined_pdb_cycle_path

refined_pdb_path_cycles[10] = "/home/abharadwaj1/papers/elife_paper/figure_information/inputs/emd_3061/model_free_proper/model_free_with_nomask/processing_files/EMD_3061_unfiltered_confidenceMap_gradient_pseudomodel_proper_element_composition.pdb"

bfactors_with_cycles = {}
for cycle, refined_pdb_path in refined_pdb_path_cycles.items():
    bfactors = get_bfactors(refined_pdb_path)
    bfactors_with_cycles[cycle] = np.array(bfactors)

# import gaussian_kde from scipy.stats
from scipy.stats import gaussian_kde
print("Plotting the B-factor distribution for each cycle")
plot_output_folder = "/home/abharadwaj1/papers/elife_paper/figure_information/scripts/supplementary_2/bfactor_distribution_cycle"

print(bfactors_with_cycles[10].min(), bfactors_with_cycles[10].max())
for cycle, bfactors in bfactors_with_cycles.items():
    # For each cycle, plot the KDE of B-factors and the fitted inverse gamma distribution
    fig, ax = plt.subplots(1, 1, figsize=(10, 5))
    # Calculate the point density
    x_grid = np.linspace(bfactors_with_cycles[10].min(), bfactors_with_cycles[10].max(), 100)
    kde = gaussian_kde(bfactors)

    # Plot the KDE
    ax.plot(x_grid, kde(x_grid), label="KDE")
    # Plot the fitted inverse gamma distribution
    a, loc, scale = invgamma.fit(bfactors)
    ax.plot(x_grid, invgamma.pdf(x_grid, a, loc, scale), label="IGD")
    ax.set_title("Cycle {}".format(cycle))

    # save the figure
    fig.savefig(os.path.join(plot_output_folder, "cycle_{}_bfactor_distribution.png".format(cycle)), dpi=600)

