# Write a script to plot the correlation curves for iterative and non-iterative pseudo-model refinement

from genericpath import isfile
import os
import sys
import shutil
import subprocess
import numpy as np
from datetime import datetime
from joblib import Parallel, delayed


import pickle
output_folder_MB = "/home/abharadwaj1/papers/elife_paper/figure_information/data/neighborhood_correlations/correlation_curves_pickle_files/correlation_curve_2_correct_MB_pickle_files"
def read_all_pickle_files():
    neighborhood_bfactor_correlation_emdbs = {}
    for pickle_file in os.listdir(output_folder_MB):
        if pickle_file.endswith(".pickle"):
            with open(os.path.join(output_folder_MB, pickle_file), "rb") as f:
                neighborhood_bfactor_correlation_emdbs[pickle_file.split("_")[0]] = pickle.load(f)
    return neighborhood_bfactor_correlation_emdbs

neighborhood_bfactor_correlation_emdbs_MB = read_all_pickle_files()


import matplotlib.pyplot as plt
from locscale.include.emmer.pdb.pdb_tools import neighborhood_bfactor_correlation_sample
from tqdm import tqdm
import seaborn as sns
plot_output_folder = "/home/abharadwaj1/papers/elife_paper/figure_information/outputs/plots_neighborhood_bfactor_analysis/all_emdb"

bfactor_neighborhood_correlations_MB_all_emdb = {}
bfactor_neighborhood_correlations_unrestrained_refinement_all_emdb = {}
for emdb in neighborhood_bfactor_correlation_emdbs_MB.keys():

    
 
    bfactor_neighborhood_correlations_MB = neighborhood_bfactor_correlation_emdbs_MB[emdb]
    curve = [x[2][0] for x in bfactor_neighborhood_correlations_MB.values()][:10]
    if np.isnan(curve).any():
        
        continue
    else:
        bfactor_neighborhood_correlations_MB_all_emdb[emdb] = curve
    
    


def pretty_lineplot_XY_multiple_with_shade(xdata_list, ydata_list, xlabel, ylabel, figsize_cm=(14,8),fontsize=10, \
                        marker="o", markersize=3,fontscale=1,font="Helvetica", \
                        linewidth=1,legends=None, title=None):
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import cm
    from locscale.include.emmer.ndimage.profile_tools import crop_profile_between_frequency
    import seaborn as sns
    import matplotlib 
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42
    # set the global font size for the plot

        
    plt.rcParams.update({'font.size': fontsize})
    figsize = (figsize_cm[0]/2.54, figsize_cm[1]/2.54) # convert cm to inches
    
    fig, ax = plt.subplots(figsize=figsize, dpi=600)  # DPI is fixed to 600 for publication quality
    sns.set_theme(context="paper", font=font, font_scale=fontscale)
    # Set font size for all text in the figure
    sns.set_style("white")

    # Plot the data by shading the mean and standard deviation for each data set
    xdata_array = np.array(xdata_list)
    ydata_array = np.array(ydata_list)

    ydata_mean = np.mean(ydata_array, axis=0)
    ydata_std = np.std(ydata_array, axis=0)

    ydata_max = ydata_mean + ydata_std
    ydata_min = ydata_mean - ydata_std

    print(xdata_array.min(), xdata_array.max())
    print(ydata_mean.min(), ydata_mean.max())
    print(ydata_min.min(), ydata_min.max())
    print(ydata_max.min(), ydata_max.max())

    # Plot the mean and standard deviation as a shaded region
    ax.fill_between(xdata_array, ydata_min, ydata_max, alpha=0.2, color="blue")
    ax.plot(xdata_array, ydata_mean, linewidth=linewidth, color="blue")

    # Add a text stating the number of data sets
    ax.text(0.75, 0.95, f"N = {ydata_array.shape[0]}", ha="left", va="top", transform=ax.transAxes)
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel, rotation=90, ha="center")
    
    plt.tight_layout()
    plt.ylim(0.2,1.2)
    plt.yticks([0.5,1.0])

    if title is not None:
        plt.title(title)
    return fig

xdata_list = np.arange(1,11)
ydata_list = [bfactor_neighborhood_correlations_MB_all_emdb[emdb_pdb] for emdb_pdb in bfactor_neighborhood_correlations_MB_all_emdb.keys()]
N = len(ydata_list)
fig = pretty_lineplot_XY_multiple_with_shade(xdata_list, ydata_list, \
                            xlabel=r"Neighborhood Radius ($\AA$)", ylabel="ADP Correlation", figsize_cm=(8,8),fontsize=8)
#%%
import json 

plt.savefig(os.path.join(plot_output_folder, "correlation_curve_MB.eps"), bbox_inches="tight")